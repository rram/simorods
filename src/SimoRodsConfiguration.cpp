// Sriramajayam

#include <SimoRodsConfiguration.h>
#include <SimoRodsUtils.h>
#include <cassert>

namespace simorods
{
  // Copy constuctor
  Configuration::Configuration(const Configuration& Obj)
    :nNodes(Obj.nNodes),  nElms(Obj.nElms),
     nQuadsPerElm(Obj.nQuadsPerElm),
     Phi(Obj.nNodes), Lambda(Obj.nElms*Obj.nQuadsPerElm),
     Omega(Obj.nElms*Obj.nQuadsPerElm), isInitialized(Obj.nNodes)
  {
    // Copy centerline coordinates
    for(int a=0; a<nNodes; ++a)
      {
	const auto& src = Obj.Phi[a];
	auto& dest = Phi[a];
	for(int i=0; i<3; ++i)
	  dest[i] = src[i];
      }
    // Copy rotations and vorticity
    for(int e=0; e<nElms; ++e)
      for(int q=0; q<nQuadsPerElm; ++q)
	{
	  const auto& srcR = Obj.Lambda[nQuadsPerElm*e+q];
	  auto& destR = Lambda[nQuadsPerElm*e+q];
	  const auto& srcO = Obj.Omega[nQuadsPerElm*e+q];
	  auto& destO = Omega[nQuadsPerElm*e+q];
	  for(int i=0; i<3; ++i)
	    for(int j=0; j<3; ++j)
	      {
		destR[i][j] = srcR[i][j];
		destO[i][j] = srcO[i][j];
	      }
	}
  }
    

  // Set the values of the centerline coordinates
  void Configuration::SetCenterline(const int a, Vec3 val)
  {
    auto& vec = Phi[a];
    for(int i=0; i<3; ++i)
      vec[i] = val[i];
    return;
  }

  // Set the values of the rotations
  void Configuration::SetRotation(const int elm, const int q, Mat3 val)
  {
    auto& mat = Lambda[nQuadsPerElm*elm+q];
    for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
	mat[i][j] = val[i][j];
    return;
  }

  // Set the values of the vorticity
  void Configuration::SetVorticity(const int elm, const int q, Mat3 val)
  {
    auto& mat = Omega[nQuadsPerElm*elm+q];
    for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
	mat[i][j] = val[i][j];
    return;
  }
  

  // Returns the centerline coordinates
  const std::vector<Vec3>& Configuration::GetCenterline() const
  { return Phi; }

  // Returns the centerline coordinates
  const Vec3& Configuration::GetCenterline(const int a) const
  { return Phi[a]; }

  // Returns the rotation tensor
  const std::vector<Mat3>& Configuration::GetRotations() const
  { return Lambda; }

  // Returns the rotation tensor
  const Mat3& Configuration::GetRotations(const int elm, const int q) const
  { return Lambda[nQuadsPerElm*elm+q]; }
  
  //! Returns the vorticity tensor
  const std::vector<Mat3>& Configuration::GetVorticity() const
  { return Omega; }

  // Returns the vorticity tensor
  const Mat3& Configuration::GetVorticity(const int elm, const int q) const
  { return Omega[nQuadsPerElm*elm+q]; }

  // Updates the centerline coordinates
  void Configuration::UpdateCenterline(const int a, const Vec3& incPhi)
  {
    auto& vec = Phi[a];
    for(int i=0; i<3; ++i)
      vec[i] += incPhi[i];
    isInitialized = false;
    return;
  }

  // Update the centerline coordinates
  void Configuration::UpdateCenterline(const std::vector<Vec3>& incPhi)
  {
    for(int a=0; a<nNodes; ++a)
      UpdateCenterline(a, incPhi[a]);
    isInitialized = false;
    return;
  }


  // Update the rotations
  void Configuration::UpdateRotations(const int elm, const int q,
				      const Vec3& theta, const Vec3& dtheta)
  {
    // Current rotation and vorticity
    Mat3 Rold = Lambda[elm*nQuadsPerElm+q];
    Mat3 Vold = Omega[elm*nQuadsPerElm+q];

    // Compute the exponential of theta and -theta
    double mtheta[] = {-theta[0], -theta[1], -theta[2]};
    double exp[3][3], mexp[3][3];
    ExpSO3(&theta[0], exp);
    ExpSO3(mtheta, mexp);

    // d/ds(Exp(theta)) Exp(-theta)
    double deriv[3][3];
    dExpSO3Proj(&theta[0], &dtheta[0], deriv);

    // Update rotations: Lambda_new = exp(theta) Lambda
    // Update vorticity: Omega_new: d/ds(exp(theta))exp(-theta) + exp(theta)Omega exp(-theta)
    auto& R = Lambda[elm*nQuadsPerElm+q];
    auto& V = Omega[elm*nQuadsPerElm+q];
    for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
	{
	  R[i][j] = 0.;
	  V[i][j] = deriv[i][j];
	  for(int k=0; k<3; ++k)
	    {
	      R[i][j] += exp[i][k]*Rold[k][j];
	      for(int L=0; L<3; ++L)
		V[i][j] += exp[i][k]*Vold[k][L]*mexp[L][j];
	    }
	}
    isInitialized = false;
    return;
  }
  
  // Update the rotations
  void Configuration::
  UpdateRotations(const std::vector<Vec3>& incTheta, const std::vector<Vec3>& incdTheta)
  {
    for(int e=0; e<nElms; ++e)
      for(int q=0; q<nQuadsPerElm; ++q)
	UpdateRotations(e, q, incTheta[nQuadsPerElm*e+q], incdTheta[nQuadsPerElm*e+q]);
    isInitialized = false;
    return;
  }
}
 
