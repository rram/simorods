// Sriramajayam

#ifndef SIMO_RODS_OPS_H
#define SIMO_RODS_OPS_H

#include <SimoRodsConfiguration.h>
#include <SimoRodsMaterial.h>
#include <SimoRodsOpsWorkspace.h>
#include <ElementalOperation.h>


namespace simorods
{
  //! Definition of a state:
  using SimoRodState = std::pair<const Configuration*, const LocalToGlobalMap*>;
  
  //! Class for computing energies, weak forms and Hessian for a
  //! STRAIGHT rod based on Simo's geometrically exact model.
  //! The reference configuration is assumed to be along Ez.
  //! Dofs are incremental displacements and  incremental vorticity
  //! These are assumed to ordered sequentially from 0-5 at each node
  class Ops: public DResidue
  {
  public:
    //! Constructor
    //! \param[in] elm Element for this operation. Same interpolation for all fields
    //! \param[in] fieldnums Field numbers within the element to use
    //! \param[in] mat Material to be used
    //! \param[in] 
    inline Ops(const Element* elm,
	       ConfigAccessStruct str,
	       const Material& mat)
      :DResidue(), Elm(elm), fields({0,1,2,3,4,5}),
      ConfigStr(str), Mat(&mat)
      { WrkSpc = new OpsWorkspace(6, Elm->GetDof(0)); }
    
    //! Destructor, destroy the workspacek
    inline virtual ~Ops()
    { delete WrkSpc; }
    
    //! Copy constructor
    //! \param[in] Obj Object to be copied
    inline Ops(const Ops& Obj)
      :DResidue(Obj),
      Elm(Obj.Elm), fields(Obj.fields), ConfigStr(Obj.ConfigStr), Mat(Obj.Mat)
      { WrkSpc = new OpsWorkspace(6, Elm->GetDof(0)); }
      

    //! Cloning
    inline virtual Ops* Clone() const override
    { return new Ops(*this); }
    
    //! Disable assignment
    Ops& operator=(const Ops&) = delete;

    //! Returns the element being used
    inline const Element* GetElement() const
    { return Elm; }

    //! Returns the material being used
    inline const Material* GetMaterial() const
    { return Mat; }

    //! Return the fields used
    inline const std::vector<int>& GetField() const override
    { return fields; }

    //! Returns the number of dofs for a given field
    inline int GetFieldDof(int fieldnumber) const override
    { return Elm->GetDof(fields[fieldnumber]); }

    //! Computes the energy functional
    //! \param[in] Config Configuration at which to compute the energy
    double GetEnergy(const void* Config) const;
    
    //! Residual calculation
    //! \param[in] Config Configuration at which to compute the residual
    //! \param[in] resval Computed local residual
    void GetVal(const void* Config,
		std::vector<std::vector<double>>* resval) const;
    
    //! Residual and stiffness calculation
    //! \param[in] Config Configuration at which to compute the residual
    //! \param[in] resval Computed local residual
    //! \param[in] dresval Computed local stiffness
    void GetDVal(const void* Config, 
		 std::vector<std::vector<double>>* resval,
		 std::vector<std::vector<std::vector<std::vector<double>>>>* dresval) const;
    
    //! Consistency test for this operation at the given configuration
    //! \param[in] Config Configuration at which to perform the consistency test
    //! \param[in] pertEPS Tolerance to use for perturbations
    //! \param[in] tolEPS Tolerance to use for examining outputs
    bool ConsistencyTest(const void* Config,
			 const double pertEPS, const double tolEPS) const;
    
  private:
    const Element* Elm; //!< Element for this operation
    const std::vector<int> fields; //!< Field numbers for this operation
    const ConfigAccessStruct ConfigStr; //!< Details to access configuration for this
    const Material* Mat; //!< Material defining this rod
    OpsWorkspace* WrkSpc; //!< Workspace to be used for calculations
  };
}


#endif
