// Sriramajayam

#ifndef SIMO_RODS_UTILS_H
#define SIMO_RODS_UTILS_H


// Utility functions
namespace simorods
{
  //! Computes the axial vector of a 3x3 skew symmetric matrix.
  //! Does not check that the given matris is skew
  //! \param[in] skw Skew symmetric matrix
  //! \param[out] theta Computed axial vector
  void AxialVector(const double skw[][3], double* theta);
  
  //! Computes the skew symmetric matrix for a given axial vector
  //! \param[in] theta Computed axial vector
  //! \param[out] skw Skew symmetric matrix
  void HodgeStar(const double* theta, double skw[][3]);
  
  //! Computes the exponential map given an axial vector
  //! \param[in] vec axial vector
  //! \param[in] Mat Computed exponential map
  void ExpSO3(const double* vec, double Mat[][3]);
  
  //! Computes the exponential map given a skew symm matrix
  //! \param[in] skw Skew-symmetric matrix
  //! \param[in] Mat Computed exponential map
  void ExpSO3(const double skw[][3], double Mat[][3]);

  //! Computes the projected derivative of the exponential map
  //! [d/dS exp(theta(S))] exp(-theta(s))
  //! \param[in] theta axial vector
  //! \param[in] dtheta derivative of axial vector
  //! \param[out] deriv Computed derivative
  void dExpSO3Proj(const double* theta, const double* dtheta,
		   double deriv[][3]);
  
}

#endif
