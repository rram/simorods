// Sriramajayam

#ifndef HERMITE_SHAPE_H
#define HERMITE_SHAPE_H

#include <Shape.h>
#include <cmath>
#include <iostream>

//! \brief Class for cubic Hermite polynomials on \f$[a,b]\f$.  
/** In the name of the class Hermite31D, the 3 stands for cubic polynomials
 * and 1D for the polynomial being defined over an interval on the real line.
 * This class defines the basis functions that interpolate the values of a function
 * and its derivative at the two end points of the element.
 * \warning Note that shape functions are computed over the real element and 
 * not the parametric element.
 */
class HermiteShape: public Shape
{
 public:
  //! Constructor
  //! \param endpts Cartesian coordinates of the end points in 1D. Should be of length 2.
  HermiteShape(const double * endpts);
  
  //! Destructor
  inline virtual ~HermiteShape() {}

  //! Copy Constructor
  HermiteShape(const HermiteShape & HShp);
  
  //! Cloning
  inline virtual HermiteShape * Clone() const override
  { return new HermiteShape(*this); }
  
  //! Returns the number of shape functions.
  inline virtual int GetNumberOfFunctions() const override
  { return 4; }
  
  //! Returns the number of arguments of the functions- which 1.
  inline virtual int GetNumberOfVariables() const override
  { return 1; }
  
  //! Computes the value of requested shape function at given point.
  //! \param a Shape function number. Should be between 0 and 3.
  //! \param x Barycentric coordinates of point. 
  //! \warning Be careful about shape functions scaling with the length of the segment.
  virtual double Val(const int a,  const double *x) const override;

  //! Computes the derivative of the requested shape function in the requested direction at given point.
  //! \param a Shape function number. Should be between 0 and 3.
  //! \param x Barycentric coordinate of point in segment.
  //! \param i Direction- Should be 0.
  //! \warning Derivatives are wrt barycentric coordinates, not cartesian coordinates.
  virtual double DVal(const int a, const double *x, const int i) const override;

  //! Computes the 2nd derivative
  //! \param a Shape function number. Should be between 0 and 3
  //! \param x Barycentric coordinates in the segment
  //! Assumes only 0 direction
  virtual double D2Val(const int a, const double* x) const;
  
 private:
  const double elmcoord[2];
  double Coef[4][4];
};

#endif
