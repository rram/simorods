// Sriramajayam

#ifndef SIMO_RODS_POINT_KINEMATICS_H
#define SIMO_RODS_POINT_KINEMATICS_H

#include <cassert>

namespace simorods
{
  // Encapsulation of kinematical data at a quadrature point
  // Coordinates of this point (in the deformed configuration)
  // Derivative of coordinates at this point (with respect 'S' in the undef config)
  // Rotation tensor
  // Derivative of the rotation tensor
  // Strain measures
  class PointKinematics
  {
  public:
    double phi[3], dphi[3];
    double Lambda[3][3], Omega[3][3];
    double Gamma[3], Kappa[3];
    double N[3], M[3];

    //! Default constructor
    inline PointKinematics() {}

    //! Copy constructor
    inline PointKinematics(const PointKinematics& Obj)
    {
      for(int i=0; i<3; ++i)
	{
	  phi[i] = Obj.phi[i];
	  dphi[i] = Obj.dphi[i];
	  for(int j=0; j<3; ++j)
	    {
	      Lambda[i][j] = Obj.Lambda[i][j];
	      Omega[i][j] = Obj.Omega[i][j];
	    }
	  Gamma[i] = Obj.Gamma[i];
	  Kappa[i] = Obj.Kappa[i];
	  N[i] = Obj.N[i];
	  M[i] = Obj.M[i];
	}
    }

    //! Destructor, does nothing
    inline virtual ~PointKinematics() {}

    //! Assignment operator
    inline PointKinematics& operator=(const PointKinematics& rhs)
      {
	if(&rhs==this) return *this;

	for(int i=0; i<3; ++i)
	  {
	    phi[i] = rhs.phi[i];
	    dphi[i] = rhs.dphi[i];
	    for(int j=0; j<3; ++j)
	      {
		Lambda[i][j] = rhs.Lambda[i][j];
		Omega[i][j] = rhs.Omega[i][j];
	      }
	    Gamma[i] = rhs.Gamma[i];
	    Kappa[i] = rhs.Kappa[i];
	    N[i] = rhs.N[i];
	    M[i] = rhs.M[i];
	  }
	return *this;
      }


    //! Set the rotation and vorticity
    template<class T>
      void SetRotationAndVorticity(const T& R, const T& V)
      {
	for(int i=0; i<3; ++i)
	  for(int j=0; j<3; ++j)
	    {
	      Lambda[i][j] = R[i][j];
	      Omega[i][j] = V[i][j];
	    }
	return;
      }
	      
		
      
    // Compute the strain measures
    inline void ComputeStrains()
    {
      // Gamma = Lambda^t dphi - E3
      // Kappa = Lambda^t omega, where omega = axial vector of Omega
      double omega[3] = {Omega[2][1], Omega[0][2], Omega[1][0]};
      
      for(int i=0; i<3; ++i)
	{
	  Gamma[i] = 0.;
	  Kappa[i] = 0.;
	  for(int j=0; j<3; ++j)
	    {
	      Gamma[i] += Lambda[j][i]*dphi[j];
	      Kappa[i] += Lambda[j][i]*omega[j];
	    }
	}
      Gamma[2] -= 1.;
      return;
    }
    
  };



  // First variation of kinematic data at a quadrature point
  class VarPointKinematics
  {
  public:
    const PointKinematics* PD; //!< Kinematic data at this point
    double eta[3], deta[3]; //!< Variation (value and derivative) of centerline
    double theta[3], dtheta[3]; //!< Variation (value and derivative) in vorticity

    double vGamma[3], vKappa[3]; //!< First variation of strain measures
    double vN[3], vM[3]; //!< First variation of stress/moment resultants

  private:
    double tempvec[3];

  public:
    
    //! Default constructor
    inline VarPointKinematics()
      :PD(nullptr)
    { SetZero(); }
    
    //! Copy constructor
    inline VarPointKinematics(const VarPointKinematics& Obj)
      :PD(Obj.PD)
    {
      for(int i=0; i<3; ++i)
	{
	  eta[i] = Obj.eta[i];
	  deta[i] = Obj.deta[i];
	  theta[i] = Obj.theta[i];
	  dtheta[i] = Obj.dtheta[i];
	  vGamma[i] = Obj.vGamma[i];
	  vKappa[i] = Obj.vKappa[i];
	  vN[i] = Obj.vN[i];
	  vM[i] = Obj.vM[i];
	}
    }

    //! Destructor, does nothing
    inline virtual ~VarPointKinematics() {}

    //! Assignment operator
    inline VarPointKinematics& operator=(const VarPointKinematics& rhs)
      { assert(false && "Should not need to assign variation of point data"); }

    //! Set all values to zero
    inline void SetZero()
    {
      for(int i=0; i<3; ++i)
	{
	  eta[i] = 0.;
	  deta[i] = 0.;
	  theta[i] = 0.;
	  dtheta[i] = 0.;
	  vGamma[i] = 0.;
	  vKappa[i] = 0.;
	  vN[i] = 0.;
	  vM[i] = 0.;
	}
    }
    
    //! Compute the first variations of strain measures
    inline void ComputeVarStrains()
    {
      const auto* dphi = PD->dphi;
      const auto* Lambda = PD->Lambda;
      
      // vGamma = Lambda^t (eta'-theta x phi')
      // vkappa = Lambda^t theta'

      // Compute theta x phi'
      for(int i=0; i<3; ++i)
	tempvec[i] = theta[(i+1)%3]*dphi[(i+2)%3] - theta[(i+2)%3]*dphi[(i+1)%3];

      // Compute variations
      for(int i=0; i<3; ++i)
	{
	  vGamma[i] = 0.;
	  vKappa[i] = 0.;
	  for(int j=0; j<3; ++j)
	    {
	      vGamma[i] += Lambda[j][i]*(deta[j]-tempvec[j]);
	      vKappa[i] += Lambda[j][i]*dtheta[j];
	    }
	}
    }
  };
  

  // Second variations of kinematic data at a quadrature point
  class VarVarPointKinematics
  {
  public:
    const PointKinematics* PD; //!< Kinematic data at this point
    const VarPointKinematics* deltaPD; //!< Variation 1
    const VarPointKinematics* DELTAPD; //!< Variation 2
    double vvGamma[3], vvKappa[3]; //! Second variations of strain measures

  private:
    double avec[3], bvec[3]; //!< Temporary vectors

  public:
    //! Default constructor
    inline VarVarPointKinematics()
      :PD(nullptr), deltaPD(nullptr), DELTAPD(nullptr)
    {
      for(int i=0; i<3; ++i)
	{ vvGamma[i] = 0.; vvKappa[i] = 0.; }
    }

    //! Copy constructor
    inline VarVarPointKinematics(const VarVarPointKinematics& Obj)
      :PD(Obj.PD), deltaPD(Obj.deltaPD), DELTAPD(Obj.DELTAPD)
      {
	for(int i=0; i<3; ++i)
	  {
	    vvGamma[i] = Obj.vvGamma[i];
	    vvKappa[i] = Obj.vvKappa[i];
	  }
      }

    //! Destructor, does nothing
    inline virtual ~VarVarPointKinematics() {}

    //! Assignment operator: disable
    inline VarVarPointKinematics* operator=(const VarVarPointKinematics& rhs)
      { assert(false && "Assignment should not be required."); }

    //! Compute the second variation of strain measures
    inline void ComputeVarVarStrains()
    {
      // Aliases from kinematics
      const auto& Lambda = PD->Lambda;
      const auto* dphi = PD->dphi;
      
      // Aliases for the first variation
      const auto* deta = deltaPD->deta;
      const auto* theta = deltaPD->theta; const auto* dtheta = deltaPD->dtheta;

      // Aliases for the second variation
      const auto* dzeta = DELTAPD->deta;
      const auto* alpha = DELTAPD->theta; 

      // cyclic permuations used in cross products
      const int ij[] = {1,2,0}; 
      const int ik[] = {2,0,1};
      
      // Second variation of Gamma: Lambda^t(eta' x alpha + zeta'xtheta + alphax(thetaxphi')
      // theta x phi'
      for(int i=0; i<3; ++i)
	avec[i] = theta[ij[i]]*dphi[ik[i]] - theta[ik[i]]*dphi[ij[i]];
     
      for(int i=0; i<3; ++i)
	bvec[i] =
	  (deta[ij[i]]*alpha[ik[i]] - deta[ik[i]]*alpha[ij[i]]) +
	  (dzeta[ij[i]]*theta[ik[i]] - dzeta[ik[i]]*theta[ij[i]]) +
	  (alpha[ij[i]]*avec[ik[i]] - alpha[ik[i]]*avec[ij[i]]);

      for(int i=0; i<3; ++i)
	{
	  vvGamma[i] = 0.;
	  for(int j=0; j<3; ++j)
	    vvGamma[i] += Lambda[j][i]*bvec[j];
	}

      // Second variation in kappa: Lambda^t(theta' x alpha)
      for(int i=0; i<3; ++i)
	avec[i] = dtheta[ij[i]]*alpha[ik[i]]-dtheta[ik[i]]*alpha[ij[i]];
      for(int i=0; i<3; ++i)
	{
	  vvKappa[i] = 0.;
	  for(int j=0; j<3; ++j)
	    vvKappa[i] += Lambda[j][i]*avec[j];
	}
      return;
    }
    
  };

}


#endif
