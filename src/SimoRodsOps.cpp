// Sriramajayam

#include <SimoRodsOps.h>
#include <SimoRodsPointKinematics.h>
#include <SimoRodsUtils.h>
#include <cmath>
#include <cassert>
#include <iostream>

namespace simorods
{
  // Computes the energy functional
  double Ops::GetEnergy(const void* arg) const
  {
    // Cast configuration
    const Configuration* Config = static_cast<const Configuration*>(arg);
    assert(Config!=nullptr && "simorods::ops::GetEnergy- Invalid pointer to configration");
    
    // Check that this configuration is "set"
    assert(Config->IsInitialized() && "Ops::GetEnergy()- Configuration not initialized");
    
    // Energy to be computed
    double Energy = 0.;

    // Access workspace for quadrature point-level calculatons
    auto& PD = WrkSpc->PD;
    
    // Number of quadrature points
    const auto& nQuads = ConfigStr.nQuadsPerElm;
    assert(nQuads==static_cast<int>(Elm->GetIntegrationWeights(fields[0]).size()));

    // Node number for this element
    const auto& nodes = ConfigStr.nodes;
    assert(static_cast<int>(nodes.size())==GetFieldDof(fields[0]));
    const int ndofs = static_cast<int>(nodes.size());
    
    // This element number
    const auto& e = ConfigStr.elm;

    // Quadrature weights
    const auto& Qwts = Elm->GetIntegrationWeights(fields[0]);
  
    // Integrate
    for(int q=0; q<nQuads; ++q)
      {
	// Centerline coordinates at this point
	for(int f=0; f<3; ++f)
	  {
	    PD.phi[f] = 0.;
	    PD.dphi[f] = 0.;
	    for(int a=0; a<ndofs; ++a)
	      {
		const auto& x = Config->GetCenterline(nodes[a]);
		PD.phi[f] += x[f]*Elm->GetShape(fields[f],q,a);
		PD.dphi[f] += x[f]*Elm->GetDShape(fields[f],q,a,0);
	      }
	  }

	// Rotations and vorticity
	const auto& Lambda = Config->GetRotations(e, q);
	const auto& Omega = Config->GetVorticity(e, q);
	for(int i=0; i<3; ++i)
	  for(int j=0; j<3; ++j)
	    {
	      PD.Lambda[i][j] = Lambda[i][j];
	      PD.Omega[i][j] = Omega[i][j];
	    }

	// Compute the strains
	PD.ComputeStrains();

	// Update the energy
	Energy += Qwts[q]*Mat->ComputeStrainEnergyDensity(PD.Gamma, PD.Kappa);
      }
    
    return Energy;
  }
    
  // Residual calculation
  void Ops::GetVal(const void* arg, 
		   std::vector<std::vector<double>>* resval) const
  { GetDVal(arg, resval, nullptr); }

  
  // Residual and stiffness calculation
  void Ops::GetDVal(const void* arg, 
		    std::vector<std::vector<double>>* resval,
		    std::vector<std::vector<std::vector<std::vector<double>>>>* dresval) const
  {
    // Cast configuration variable
    const Configuration* Config = static_cast<const Configuration*>(arg);
    assert(Config!=nullptr && "simorods::Ops::GetDVal- Invalid configuration pointer");
    
    // Check that this provided configuration has been set
    assert(Config->IsInitialized() &&
	   "simorods::Ops::GetDResidue()- Configuration not initialized.");

    // initialize residual and stiffness to zero
    SetZero(resval, dresval);
    
    // Assume that at least one of the residual/stiffness is required
    assert((resval!=nullptr || dresval!=nullptr) &&
	   "simorods::Ops::GetDResidue()- Residual is assumed to be required");
    
    // Access the workspace 
    auto& PD = WrkSpc->PD;
    auto& varPD = WrkSpc->varPD;
    auto& VARvar = WrkSpc->varvarPD;
    auto& tempval = WrkSpc->tempval;

    // Number of quadrature points
    const auto& nQuad = ConfigStr.nQuadsPerElm;

    // Quadrature weights
    const auto& Qwts = Elm->GetIntegrationWeights(fields[0]);
    assert(static_cast<int>(Qwts.size())==nQuad);
    
    // Access details for configuration
    const auto& elm = ConfigStr.elm;
    const auto& nodes = ConfigStr.nodes;
    const int ndofs = static_cast<int>(nodes.size());
    assert(ndofs==GetFieldDof(fields[0]) && "simorods::Ops::GetDResidue- Inconsistent number of dofs");

    // Integrate
    for(int q=0; q<nQuad; ++q)
      {
	// Set the configuration at this point
	// centerline
	for(int f=0; f<3; ++f)
	  {
	    PD.phi[f] = 0.;
	    PD.dphi[f] = 0.;
	    for(int a=0; a<ndofs; ++a)
	      {
		PD.phi[f] += Config->GetCenterline(nodes[a])[f]*Elm->GetShape(fields[f],q,a);
		PD.dphi[f] += Config->GetCenterline(nodes[a])[f]*Elm->GetDShape(fields[f],q,a,0);
	      }
	  }
	// rotations and vorticity
	const auto& Lambda = Config->GetRotations(elm, q);
	const auto& Omega = Config->GetVorticity(elm, q);
	for(int i=0; i<3; ++i)
	  for(int j=0; j<3; ++j)
	    {
	      PD.Lambda[i][j] = Lambda[i][j];
	      PD.Omega[i][j] = Omega[i][j];
	    }
	
	// Compute the strains
	PD.ComputeStrains();

	// Compute the material response
	Mat->ComputeResultants(PD.Gamma, PD.Kappa, PD.N, PD.M);

	// Set centerline  variations
	for(int f=0; f<3; ++f)
	  for(int a=0; a<ndofs; ++a)
	    {
	      auto& var = varPD[f][a];
	      var.SetZero();
	      var.PD = &PD;
	      var.eta[f] = Elm->GetShape(fields[f],q,a);
	      var.deta[f] = Elm->GetDShape(fields[f],q,a,0);

	      // Compute the variations of strains
	      var.ComputeVarStrains();

	      // Compute variations of stress/moment resultants
	      Mat->ComputeVarResultants(PD.Gamma, PD.Kappa,
					var.vGamma, var.vKappa,
					var.vN, var.vM);
	    }

	// Set vorticity variations
	for(int f=0; f<3; ++f)
	  for(int a=0; a<ndofs; ++a)
	    {
	      auto& var = varPD[f+3][a];
	      var.SetZero();
	      var.PD = &PD;
	      var.theta[f] = Elm->GetShape(fields[f+3],q,a);
	      var.dtheta[f] = Elm->GetDShape(fields[f+3],q,a,0);

	      // Compute variations of strains
	      var.ComputeVarStrains();

	      // Compute variations of stress/moment resultants
	      Mat->ComputeVarResultants(PD.Gamma, PD.Kappa,
					var.vGamma, var.vKappa,
					var.vN, var.vM);
	    }

	// Compute the residue
	if(resval!=nullptr)
	  for(int f=0; f<6; ++f)
	    for(int a=0; a<ndofs; ++a)
	      {
		auto& var = varPD[f][a];

		// N.delta(Gamma) + M.delta(Kappa)
		tempval = 0.;
		for(int i=0; i<3; ++i)
		  tempval += PD.N[i]*var.vGamma[i] + PD.M[i]*var.vKappa[i];

		// Update residue
		(*resval)[fields[f]][a] += Qwts[q]*tempval;
	      }

	// Compute the stiffness
	if(dresval!=nullptr)
	  for(int f=0; f<6; ++f)
	    for(int a=0; a<ndofs; ++a)
	      {
		const auto& var = varPD[f][a];
		for(int g=0; g<6; ++g)
		  for(int b=0; b<ndofs; ++b)
		    {
		      const auto& VAR = varPD[g][b];
		      
		      // 2nd variation
		      VARvar.PD = &PD;
		      VARvar.deltaPD = &var;
		      VARvar.DELTAPD = &VAR;

		      // Compute second variations of strains
		      VARvar.ComputeVarVarStrains();

		      // VAR( N.delta(Gamma) + M.delta(Kappa) )
		      tempval = 0.;
		      for(int i=0; i<3; ++i)
			tempval +=
			  VAR.vN[i]*var.vGamma[i] + VAR.vM[i]*var.vKappa[i] +
			  PD.N[i]*VARvar.vvGamma[i] + PD.M[i]*VARvar.vvKappa[i];

		      // Update the stiffness
		      (*dresval)[fields[f]][a][fields[g]][b] += Qwts[q]*tempval;
		    }
	      }
      }
    return;
  }
    

  // Consistency test for this operation at the given configuration
  bool Ops::ConsistencyTest(const void* arg,
			    const double pertEPS, const double tolEPS) const
  {
    // Cast configuration pointer
    const Configuration* ptr = static_cast<const Configuration*>(arg);
    assert(ptr!=nullptr && "simorods::Ops::ConsistencyTest- invalid configuration pointer");
    const Configuration& Config = *ptr;
    assert(Config.IsInitialized() && "simorods::Ops::ConsistencyTest- configuration not initialized");
    
    // Compute the residue and the derivative
    const int ndofs = GetFieldDof(0);
    const int nfields = static_cast<int>(fields.size());
    assert(static_cast<int>(fields.size())==6);
    std::vector<std::vector<double>> resvals(nfields, std::vector<double>(ndofs));
    std::vector<std::vector<std::vector<std::vector<double>>>>dresvals(nfields);
    for(int f=0; f<nfields; ++f)
      {
	dresvals[f].resize(ndofs);
	for(int a=0; a<ndofs; ++a)
	  {
	    dresvals[f][a].resize(nfields);
	    for(int g=0; g<nfields; ++g)
	      dresvals[f][a][g].resize(ndofs);
	  }
      }
    GetDVal(&Config, &resvals, &dresvals);

    // Compute the residue and dresidue numerically
    auto nresvals = resvals;
    auto ndresvals = dresvals;
    SetZero(&nresvals, &ndresvals);

    // Local residual calculations at perturbed configs
    auto pres = resvals;
    auto mres = resvals;
    
    // Compute the residual/dresidual numerically
    // Fields for centerline
    for(int f=0; f<3; ++f) 
      for(int a=0; a<ndofs; ++a)
	{
	  // Create perturbed configurations here
	  Configuration pConfig(Config), mConfig(Config);

	  // Centerline update
	  Vec3 incPhi;
	  for(int i=0; i<3; ++i)
	    incPhi[i] = 0.;
	  incPhi[f] = pertEPS;
	  pConfig.UpdateCenterline(ConfigStr.nodes[a], incPhi);
	  incPhi[f] = -pertEPS;
	  mConfig.UpdateCenterline(ConfigStr.nodes[a], incPhi);

	  // No rotation/vorticity updates
	  pConfig.SetInitialized();
	  mConfig.SetInitialized();

	  // Compute perturbed energies
	  auto Eplus = GetEnergy(&pConfig);
	  auto Eminus = GetEnergy(&mConfig);
	  nresvals[f][a] = (Eplus-Eminus)/(2.*pertEPS);

	  // Compute the perturbed residual
	  GetVal(&mConfig, &mres);
	  GetVal(&pConfig, &pres);

	  // Update the numerical stiffnesses
	  for(int g=0; g<6; ++g)
	    for(int b=0; b<ndofs; ++b)
	      ndresvals[g][b][f][a] = (pres[g][b]-mres[g][b])/(2.*pertEPS);
	}
    
    // Fields for vorticity
    for(int f=0; f<3; ++f)
      for(int a=0; a<ndofs; ++a)
	{
	  // Create perturbed configurations
	  Configuration pConfig(Config), mConfig(Config);

	  // Update rotations and vorticities at quadrature points
	  for(int q=0; q<ConfigStr.nQuadsPerElm; ++q)
	    {
	      Vec3 theta, dtheta;
	      for(int i=0; i<3; ++i)
		{ theta[i] = dtheta[i] = 0.; }
	      theta[f] = pertEPS*Elm->GetShape(fields[f+3], q, a);
	      dtheta[f] = pertEPS*Elm->GetDShape(fields[f+3], q, a, 0);
	      pConfig.UpdateRotations(ConfigStr.elm, q, theta, dtheta);
	      theta[f] *= -1.;
	      dtheta[f] *= -1.;
	      mConfig.UpdateRotations(ConfigStr.elm, q, theta, dtheta);
	    }
	  
	  // No centerline updates
	  pConfig.SetInitialized();
	  mConfig.SetInitialized();
	  
	  // Compute perturbed energies
	  auto Eplus = GetEnergy(&pConfig);
	  auto Eminus = GetEnergy(&mConfig);
	  nresvals[f+3][a] = (Eplus-Eminus)/(2.*pertEPS);

	  // Compute the residuals at the perturbed configuration
	  GetVal(&mConfig, &mres);
	  GetVal(&pConfig, &pres);

	  // numerical stiffness values
	   for(int g=0; g<nfields; ++g)
	     for(int b=0; b<ndofs; ++b)
	       ndresvals[g][b][f+3][a] = (pres[g][b]-mres[g][b])/(2.*pertEPS);
	}

    // Check consistency
    for(int f=0; f<6; ++f)
      for(int a=0; a<ndofs; ++a)
	{
	  assert(std::abs(resvals[f][a]-nresvals[f][a])<tolEPS &&
		 "simorods::Ops::Consistency test for residuals failed");
	  for(int g=0; g<6; ++g)
	    for(int b=0; b<ndofs; ++b)
	      assert(std::abs(dresvals[f][a][g][b]-ndresvals[f][a][g][b])<tolEPS &&
		     "simorods::Ops::Consistency test for dresiduals failed");
	}
		
	  
    // For debugging puposes only
    if(0)
      {
	std::cout<<"\n\nConsistency of residuals: ";
	for(int f=0; f<6; ++f)
	  for(int a=0; a<ndofs; ++a)
	    std::cout<<"\n"<<resvals[f][a]<<" should be "<<nresvals[f][a];
	std::fflush( stdout );

	std::cout<<"\n\nConsistency of dresiduals: ";
	for(int f=0; f<6; ++f)
	  for(int a=0; a<ndofs; ++a)
	    for(int g=0; g<6; ++g)
	      for(int b=0; b<ndofs; ++b)
		std::cout<<"\n"<<dresvals[f][a][g][b]<<" should be "<<ndresvals[f][a][g][b];
	std::fflush( stdout );
      }
    return true;  
  }
  
}
