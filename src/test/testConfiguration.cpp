// Sriramajayam

#include <iostream>
#include <SimoRodsConfiguration.h>
#include <SimoRodsPointKinematics.h>
#include <SimoRodsUtils.h>
#include <random>
#include <cassert>

using namespace simorods;

// Random double 
std::random_device rd;  //Will be used to obtain a seed for the random number engine
std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
std::uniform_real_distribution<> dis(-1.,1.);

// Generate a random vector
Vec3 GenerateVector();

// Generate a random rotation
Mat3 GenerateRotation();

// Generate a random skew symm matrix
Mat3 GenerateSkew();

// Check copy constructor
void  CheckCopy(const Configuration& config);

// Consistency test for centerline/rotation/vorticity updates for a 1-point config.
void ConsistencyTest(const Configuration& PtConfig);

// Determinant of 3x3 matrix
double Det(const Mat3 mat);


int main()
{
  const int nNodes = 3;
  const int nElms = 2;
  const int nQuadsPerElm = 4;
  Configuration config(nNodes, nElms, nQuadsPerElm);
  assert(!config.IsInitialized());
  assert(nNodes==static_cast<int>(config.GetCenterline().size()));
  assert(nElms*nQuadsPerElm==static_cast<int>(config.GetRotations().size()));
  assert(nElms*nQuadsPerElm==static_cast<int>(config.GetVorticity().size()));
  assert(nNodes==config.GetNumNodes());
  assert(nElms==config.GetNumElements());
  assert(nQuadsPerElm==config.GetNumQuadraturePointsPerElement());
  
  // Intialize centerline coordinates
  Vec3 vec;
  for(int a=0; a<nNodes; ++a)
    {
      vec = GenerateVector();
      config.SetCenterline(a, vec);
      auto& phi = config.GetCenterline(a);
      assert(std::abs(vec[0]-phi[0])+std::abs(vec[1]-phi[1])+std::abs(vec[2]-phi[2])<1.e-8);
    }
  
  // Initialize rotations & vorticity
  Mat3 mat;
  for(int e=0; e<nElms; ++e)
    for(int q=0; q<nQuadsPerElm; ++q)
      {
	// Rotations
	mat = GenerateRotation();
	config.SetRotation(e, q, mat);
	auto& R = config.GetRotations(e,q);
	for(int i=0; i<3; ++i)
	  for(int j=0; j<3; ++j)
	    assert(std::abs(R[i][j]-mat[i][j])<1.e-8);
	
	// Vorticity
	mat = GenerateSkew();
	config.SetVorticity(e, q, mat);
	auto& V = config.GetVorticity(e,q);
	for(int i=0; i<3; ++i)
	  for(int j=0; j<3; ++j)
	    assert(std::abs(V[i][j]-mat[i][j])<1.e-8);
      }

  // Set the configuration as initialized
  config.SetInitialized();
  assert(config.IsInitialized());

  // Check copy constructor
  CheckCopy(config);


  // Create a point configuration to be used for consistency tests
  Configuration PtConfig(1,1,1);
  vec = GenerateVector();
  PtConfig.SetCenterline(0, vec);
  mat = GenerateRotation();
  PtConfig.SetRotation(0,0,mat);
  mat = GenerateSkew();
  PtConfig.SetVorticity(0,0,mat);
  PtConfig.SetInitialized();
  
  // Check consistency of updates
  ConsistencyTest(PtConfig);
}


// Generate a random vector
Vec3 GenerateVector()
{
  Vec3 vec;
  for(int i=0; i<3; ++i)
    vec[i] = dis(gen);
  return vec;
}

// Generate a random rotation
Mat3 GenerateRotation()
{
  // Generate a random rotation tensor using the axis-angle representation
  double theta = dis(gen);
  double e[3];
  while(true)
    {
      double norm = 0.;
      for(int i=0; i<3; ++i)
	{
	  e[i] = dis(gen);
	  norm +=e[i]*e[i];
	}
      norm = sqrt(norm);
      if(norm>1.e-2)
	{
	  for(int i=0; i<3; ++i)
	    e[i] /= norm;
	  break;
	}
    }

  // Create the rotation matrix
  double StarE[3][3];
  HodgeStar(e, StarE);
  double delta[3][3] = {{1.,0.,0.},{0.,1.,0.},{0.,0.,1.}};
  Mat3 R;
  for(int i=0; i<3; ++i)
    for(int j=0; j<3; ++j)
      R[i][j] =
	e[i]*e[j] + (delta[i][j]-e[i]*e[j])*cos(theta) + StarE[i][j]*sin(theta);
  return R;
}

// Generate a random skew symm matrix
Mat3 GenerateSkew()
{
  Vec3 vec = GenerateVector();
  double skw[3][3];
  HodgeStar(&vec[0], skw);
  Mat3 mat;
  for(int i=0; i<3; ++i)
    for(int j=0; j<3; ++j)
      mat[i][j] = skw[i][j];
  return mat;
}
   
// Check copy constructor
void  CheckCopy(const Configuration& config)
{
  Configuration copy(config);
  assert(config.GetCenterline().size()==copy.GetCenterline().size());
  assert(config.GetRotations().size()==copy.GetRotations().size());
  assert(config.GetVorticity().size()==copy.GetVorticity().size());
  assert(config.GetNumNodes()==copy.GetNumNodes());
  assert(config.GetNumElements()==copy.GetNumElements());
  assert(config.GetNumQuadraturePointsPerElement()==copy.GetNumQuadraturePointsPerElement());
  
  const int nNodes = static_cast<int>(config.GetCenterline().size());
  const int nElms = config.GetNumElements();
  const int nQuadsPerElm = config.GetNumQuadraturePointsPerElement();
  for(int a=0; a<nNodes; ++a)
    {
      auto& vec1 = config.GetCenterline(a);
      auto& vec2 = copy.GetCenterline(a);
      for(int i=0; i<3; ++i)
	assert(std::abs(vec1[i]-vec2[i])<1.e-8);
    }
  for(int e=0; e<nElms; ++e)
    for(int q=0; q<nQuadsPerElm; ++q)
      {
	auto& mat1 = config.GetRotations(e, q);
	auto& mat2 = copy.GetRotations(e, q);
	auto& mat3 = config.GetVorticity(e, q);
	auto& mat4 = copy.GetVorticity(e, q);
	for(int i=0; i<3; ++i)
	  for(int j=0; j<3; ++j)
	    {
	      assert(std::abs(mat1[i][j]-mat2[i][j])<1.e-8);
	      assert(std::abs(mat3[i][j]-mat4[i][j])<1.e-8);
	    }
      }
}



// Consistency test
void ConsistencyTest(const Configuration& PtConfig)
{
  // Generate a random value for dphi
  // Since the configuration does not store dphi, update dphi in place.
  auto dphi = GenerateVector();
  
  // Point data at this configuration
  PointKinematics PD;
  for(int i=0; i<3; ++i)
    {
      PD.phi[i] = PtConfig.GetCenterline(0)[i];
      PD.dphi[i] = dphi[i];
      for(int j=0; j<3; ++j)
	{
	  PD.Lambda[i][j] = PtConfig.GetRotations(0,0)[i][j];
	  PD.Omega[i][j] = PtConfig.GetVorticity(0,0)[i][j];
	}
    }

  // Create a random variation
  VarPointKinematics var;
  var.PD = &PD;
  auto eta = GenerateVector();
  auto deta = GenerateVector();
  auto theta = GenerateVector();
  auto dtheta = GenerateVector();
  for(int i=0; i<3; ++i)
    {
      var.eta[i] = eta[i];
      var.deta[i] = deta[i];
      var.theta[i] = theta[i];
      var.dtheta[i] = dtheta[i];
    }
  var.ComputeVarStrains();
	

  // Neighboring configurations for this variation
  const double EPS = 1.e-5;
  Configuration PConfig(PtConfig), MConfig(PtConfig);
  Vec3 Pdphi, Mdphi; // Updated dphi values
  // Positive perturbation
  for(int i=0; i<3; ++i)
    {
      eta[i] *= EPS; deta[i] *= EPS;
      theta[i] *= EPS; dtheta[i] *= EPS;
      Pdphi[i] = dphi[i]+deta[i];
    }
  PConfig.UpdateCenterline(0,eta);
  PConfig.UpdateRotations(0,0,theta,dtheta);
  PConfig.SetInitialized();
  assert(std::abs(Det(PConfig.GetRotations(0,0))-1.)<1.e-7);
  assert(std::abs(Det(PConfig.GetVorticity(0,0)))<1.e-7);

  // Point data for positive perturbation
  PointKinematics PDplus;
  for(int i=0; i<3; ++i)
    {
      PDplus.phi[i] = PConfig.GetCenterline(0)[i];
      PDplus.dphi[i] = Pdphi[i];
      for(int j=0; j<3; ++j)
	{
	  PDplus.Lambda[i][j] = PConfig.GetRotations(0,0)[i][j];
	  PDplus.Omega[i][j] = PConfig.GetVorticity(0,0)[i][j];
	}
    }
  PDplus.ComputeStrains();
  
  // Negative perturbation
  for(int i=0; i<3; ++i)
    {
      eta[i] *= -1.; deta[i] *= -1.;
      theta[i] *= -1.; dtheta[i] *= -1.;
      Mdphi[i] = dphi[i]+deta[i];
    }
  MConfig.UpdateCenterline(0,eta);
  MConfig.UpdateRotations(0,0,theta,dtheta);
  MConfig.SetInitialized();
  assert(std::abs(Det(MConfig.GetRotations(0,0))-1.)<1.e-7);
  assert(std::abs(Det(MConfig.GetVorticity(0,0)))<1.e-7);

  // Point data for negative perturbation
  PointKinematics PDminus;
  for(int i=0; i<3; ++i)
    {
      PDminus.phi[i] = MConfig.GetCenterline(0)[i];
      PDminus.dphi[i] = Mdphi[i];
      for(int j=0; j<3; ++j)
	{
	  PDminus.Lambda[i][j] = MConfig.GetRotations(0,0)[i][j];
	  PDminus.Omega[i][j] = MConfig.GetVorticity(0,0)[i][j];
	}
    }
  PDminus.ComputeStrains();

  // Check consistency of strain variations
  for(int i=0; i<3; ++i)
    {
      auto val = var.vGamma[i];
      auto nval = (PDplus.Gamma[i]-PDminus.Gamma[i])/(2.*EPS);
      assert(std::abs(val-nval)<10.*EPS);
    }
  for(int i=0; i<3; ++i)
    {
      auto val = var.vKappa[i];
      auto nval = (PDplus.Kappa[i]-PDminus.Kappa[i])/(2.*EPS);
      assert(std::abs(val-nval)<10.*EPS);
    }
}

// Determinant of 3x3 matrix
double Det(const Mat3 Mat)
{
  double det = 0.;
  for(int i=0; i<3; ++i)
    det += Mat[0][i]*(Mat[1][(i+1)%3]*Mat[2][(i+2)%3]-Mat[1][(i+2)%3]*Mat[2][(i+1)%3]);
  return det;
}

