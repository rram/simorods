// Sriramajayam

#include <cassert>
#include <iostream>
#include <SimoRodsP11DElement.h>
#include <SimoRodsOps.h>
#include <SimoRodsUtils.h>
#include <random>

using namespace simorods;

// Random double 
std::random_device rd;  //Will be used to obtain a seed for the random number engine
std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
std::uniform_real_distribution<> dis(-1.,1.);

// Generate a random vector
Vec3 GenerateVector();

// Generate a random rotation
Mat3 GenerateRotation();

// Generate a random skew symm matrix
Mat3 GenerateSkew();

int main()
{
  // Create a 1-element configuration
  const int nNodes = 2;
  const int nElms = 1;
  const int nQuadsPerElm = 2;
  Configuration Config(nNodes, nElms, nQuadsPerElm);

  // Set centerline coordinates
  for(int a=0; a<nNodes; ++a)
    {
      auto vec = GenerateVector();
      Config.SetCenterline(a, vec);
    }
  // Set rotations and vorticity
  for(int q=0; q<nQuadsPerElm; ++q)
    {
      Mat3 R = GenerateRotation();
      Config.SetRotation(0, q, R);
      Mat3 V = GenerateSkew();
      Config.SetVorticity(0, q, V);
    }
    Config.SetInitialized();

    // Create 1 element
    std::vector<double> Coordinates({0.25,1.49});
    Segment<1>::SetGlobalCoordinatesArray(Coordinates);
    const int nFields = 6;
    P11DElement<nFields> Elm(0, 1);

    // Material
    MatConstants MC({2.5+dis(gen),2.5+dis(gen),2.5+dis(gen),
	  2.5+dis(gen), 2.5+dis(gen), 2.5+dis(gen)});// GA1, GA2, EA, EI1, EI2, GJ
    Material Mat(MC);
    
    // Workspace for operations
    const int ndofs = Elm.GetDof(0);
    ConfigAccessStruct Str(std::vector<int>({0,1}), 0, nQuadsPerElm);
    
    // Create an operation
    Ops Op(&Elm, Str, Mat);

    // Compute the energy, residual and dresidual at the given configuration
    std::vector<std::vector<double>> resval(nFields, std::vector<double>(ndofs));
    std::vector<std::vector<std::vector<std::vector<double>>>> dresval(nFields);
    for(int f=0; f<nFields; ++f)
      {
	dresval[f].resize(ndofs);
	for(int a=0; a<ndofs; ++a)
	  {
	    dresval[f][a].resize(nFields);
	    for(int g=0; g<nFields; ++g)
	      dresval[f][a][g].resize(ndofs);
	  }
      }
    Op.GetEnergy(&Config); // return value ignored
    Op.GetVal(&Config, &resval);
    Op.GetDVal(&Config, &resval, &dresval);

    // Consistency test
    assert(Op.ConsistencyTest(&Config, 1.e-5, 1.e-5));
}


// Generate a random vector
Vec3 GenerateVector()
{
  Vec3 vec;
  for(int i=0; i<3; ++i)
    vec[i] = dis(gen);
  return vec;
}

// Generate a random rotation
Mat3 GenerateRotation()
{
  // Generate a random rotation tensor using the axis-angle representation
  double theta = dis(gen);
  double e[3];
  while(true)
    {
      double norm = 0.;
      for(int i=0; i<3; ++i)
	{
	  e[i] = dis(gen);
	  norm +=e[i]*e[i];
	}
      norm = sqrt(norm);
      if(norm>1.e-2)
	{
	  for(int i=0; i<3; ++i)
	    e[i] /= norm;
	  break;
	}
    }

  // Create the rotation matrix
  double StarE[3][3];
  HodgeStar(e, StarE);
  double delta[3][3] = {{1.,0.,0.},{0.,1.,0.},{0.,0.,1.}};
  Mat3 R;
  for(int i=0; i<3; ++i)
    for(int j=0; j<3; ++j)
      R[i][j] =
	e[i]*e[j] + (delta[i][j]-e[i]*e[j])*cos(theta) + StarE[i][j]*sin(theta);
  return R;
}

// Generate a random skew symm matrix
Mat3 GenerateSkew()
{
  Vec3 vec = GenerateVector();
  double skw[3][3];
  HodgeStar(&vec[0], skw);
  Mat3 mat;
  for(int i=0; i<3; ++i)
    for(int j=0; j<3; ++j)
      mat[i][j] = skw[i][j];
  return mat;
}
   
