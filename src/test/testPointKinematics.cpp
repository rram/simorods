// Sriramajayam

#include <iostream>
#include <SimoRodsPointKinematics.h>
#include <random>
#include <gsl/gsl_sf_trig.h>

// Random double 
std::random_device rd;  //Will be used to obtain a seed for the random number engine
std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
std::uniform_real_distribution<> dis(-1.,1.);

using namespace simorods;

// Initialize values
void Initialize(PointKinematics& PD);

// Update state
void Update(PointKinematics& PD, const VarPointKinematics& var);

// Compute numerical values for first variations
void ComputeNumericalVariations(const PointKinematics& PD, const VarPointKinematics& var,
			       const double EPS, double* varGamma, double* varKappa);

// Compute numerical values for second variations
void ComputeNumerical2ndVariations(const PointKinematics& PD,
				   const VarPointKinematics& var, const VarPointKinematics& VAR,
				   const double EPS, double* vvGamma, double* vvKappa);

// Toggle to print results
const bool PRINT = false;

int main()
{
  // Initialize point data
  PointKinematics PD;
  Initialize(PD);

  // Compute the strains
  PD.ComputeStrains();
  
  // Set up two independent variations
  VarPointKinematics varA;
  varA.PD = &PD;
  for(int i=0; i<3; ++i)
    { varA.eta[i] = dis(gen); varA.deta[i] = dis(gen);
      varA.theta[i] = dis(gen); varA.dtheta[i] = dis(gen); }
  varA.ComputeVarStrains();

  VarPointKinematics varB;
  varB.PD = &PD;
  for(int i=0; i<3; ++i)
    { varB.eta[i] = dis(gen); varB.deta[i] = dis(gen);
      varB.theta[i] = dis(gen); varB.dtheta[i] = dis(gen); }
  varB.ComputeVarStrains();

  // Compute the first variations numerically
  const double EPS = 1.e-5;

  // Variation A
  double varAGamma[3], varAKappa[3];
  ComputeNumericalVariations(PD, varA, EPS, varAGamma, varAKappa);
  if(PRINT)
    {
      std::cout<<"\nVariation of Gamma: ";
      for(int i=0; i<3; ++i)
	std::cout<<"\n"<<varA.vGamma[i]<<" should be "<<varAGamma[i];
      std::cout<<"\nVariation of Kappa: ";
      for(int i=0; i<3; ++i)
	std::cout<<"\n"<<varA.vKappa[i]<<" should be "<<varAKappa[i];
      std::cout<<"\n\n"; std::fflush( stdout );
    }
  for(int i=0; i<3; ++i)
    assert(std::abs(varA.vGamma[i]-varAGamma[i])+std::abs(varA.vKappa[i]-varAKappa[i])<1.e-4);
  
  // Variation B
  double varBGamma[3], varBKappa[3];
  ComputeNumericalVariations(PD, varB, EPS, varBGamma, varBKappa);
  if(PRINT)
    {
      std::cout<<"\nVariation of Gamma: ";
      for(int i=0; i<3; ++i)
	std::cout<<"\n"<<varB.vGamma[i]<<" should be "<<varBGamma[i];
      std::cout<<"\nVariation of Kappa: ";
      for(int i=0; i<3; ++i)
	std::cout<<"\n"<<varB.vKappa[i]<<" should be "<<varBKappa[i];
      std::cout<<"\n\n"; std::fflush( stdout );
    }
  for(int i=0; i<3; ++i)
    assert(std::abs(varB.vGamma[i]-varBGamma[i])+std::abs(varB.vKappa[i]-varBKappa[i])<1.e-4);


  // Second variation
  VarVarPointKinematics varAB;
  varAB.PD = &PD;
  varAB.deltaPD = &varA;
  varAB.DELTAPD = &varB;
  varAB.ComputeVarVarStrains();

  // Check consistency of second variations
  double vvGamma[3], vvKappa[3];
  ComputeNumerical2ndVariations(PD, varA, varB, EPS, vvGamma, vvKappa);
  if(PRINT)
    {
      std::cout<<"\nSecond variation of Gamma: ";
      for(int i=0; i<3; ++i)
	std::cout<<"\n"<<varAB.vvGamma[i]<<" should be "<<vvGamma[i];
      std::cout<<"\nSecond variation of Kappa: ";
      for(int i=0; i<3; ++i)
	std::cout<<"\n"<<varAB.vvKappa[i]<<" should be "<<vvKappa[i];
      std::cout<<"\n\n"; std::fflush( stdout );
    }
  for(int i=0; i<3; ++i)
    assert(std::abs(varAB.vvGamma[i]-vvGamma[i])+std::abs(varAB.vvKappa[i]-vvKappa[i])<1.e-4);
  
}

// Returns the axial vector of a skew symmetric matrix
void AxialVector(const double E[][3], double* e)
{ e[0] = E[2][1]; e[1] = E[0][2]; e[2] = E[1][0]; }


// Returns the hodge star of a vector
void HodgeStar(const double* e, double E[][3])
{
  for(int i=0; i<3; ++i)
    for(int j=0; j<3; ++j)
      E[i][j] = 0.;
  E[0][1] = -e[2]; E[1][0] = e[2];
  E[0][2] =  e[1]; E[2][0] = -e[1];
  E[2][1] =  e[0]; E[1][2] = -e[0];

  // Check that the axial vector of E is e.
  double axial[3];
  AxialVector(E, axial);
  assert(std::abs(axial[0]-e[0])+std::abs(axial[1]-e[1])+std::abs(axial[2]-e[2])<1.e-6);
  return;
}



// Initialize values
void Initialize(PointKinematics& PD)
{
  // Set random values for phi and phi'
  for(int i=0; i<3; ++i)
    {
      PD.phi[i] = dis(gen);
      PD.dphi[i] = dis(gen);
    }

  // Generate a random rotation tensor using the axis-angle representation
  double theta = dis(gen);
  double e[3];
  while(true)
    {
      double norm = 0.;
      for(int i=0; i<3; ++i)
	{
	  e[i] = dis(gen);
	  norm +=e[i]*e[i];
	}
      norm = sqrt(norm);
      if(norm>1.e-2)
	{
	  for(int i=0; i<3; ++i)
	    e[i] /= norm;
	  break;
	}
    }

  // Create the rotation matrix
  double StarE[3][3];
  HodgeStar(e, StarE);
  double delta[3][3] = {{1.,0.,0.},{0.,1.,0.},{0.,0.,1.}};
  for(int i=0; i<3; ++i)
    for(int j=0; j<3; ++j)
      PD.Lambda[i][j] =
	e[i]*e[j] + (delta[i][j]-e[i]*e[j])*cos(theta) + StarE[i][j]*sin(theta);

  // Check that the rotation matrix has determinant 1
  double det = 0.;
  for(int i=0; i<3; ++i)
    det += PD.Lambda[0][i]*
      (PD.Lambda[1][(i+1)%3]*PD.Lambda[2][(i+2)%3]-PD.Lambda[1][(i+2)%3]*PD.Lambda[2][(i+1)%3]);
  assert(std::abs(det-1.)<1.e-6);

  // Create the Omega matrix
  double omega[] = {dis(gen)/3., dis(gen)/3., dis(gen)/3.};
  HodgeStar(omega, PD.Omega);
}

// Exponential map
void ExpSO3(const double* theta, double Mat[][3])
{
  double Skw[3][3];
  HodgeStar(theta, Skw);

  double angle = sqrt(theta[0]*theta[0]+theta[1]*theta[1]+theta[2]*theta[2]);
  double sincx = gsl_sf_sinc(angle);
  double sincxby2 = gsl_sf_sinc(angle/2.);
  const double delta[3][3] = {{1.,0.,0.},{0.,1.,0.},{0.,0.,1.}};

  // I + sinc(x) Skw + (1-cos(x))/x^2 Skw^2
  for(int i=0; i<3; ++i)
    for(int j=0; j<3; ++j)
      {
	Mat[i][j] = 0.;
	for(int k=0; k<3; ++k)
	  Mat[i][j] += Skw[i][k]*Skw[k][j];
	Mat[i][j] *= 0.5*sincxby2*sincxby2;
	Mat[i][j] += (delta[i][j] + sincx*Skw[i][j]);
      }

  // Check that ExpSO3 has det ~ 1
  double det = 0.;
  for(int i=0; i<3; ++i)
    det += Mat[0][i]*(Mat[1][(i+1)%3]*Mat[2][(i+2)%3]-Mat[1][(i+2)%3]*Mat[2][(i+1)%3]);
  assert(std::abs(det-1.)<1.e-6);
  
  return;
}
    

// Update state
void Update(PointKinematics& PD, const VarPointKinematics& var)
{
  // Aliases
  auto* phi = PD.phi;
  auto* dphi = PD.dphi;
  auto* Lambda = PD.Lambda;
  auto* Omega = PD.Omega;
  
  const auto* eta = var.eta;
  const auto* deta = var.deta;
  const auto* theta = var.theta;
  const auto* dtheta = var.dtheta;

  // Update rotations Lambda = exp(Skw(theta)) Lambda
  double exp[3][3];
  ExpSO3(theta, exp);
  double LambdaNew[3][3];
  for(int i=0; i<3; ++i)
    for(int j=0; j<3; ++j)
      {
	LambdaNew[i][j] = 0.;
	for(int k=0; k<3; ++k)
	  LambdaNew[i][j] += exp[i][k]*Lambda[k][j];
      }
  
  // thetabar
  double angle = sqrt(theta[0]*theta[0] + theta[1]*theta[1] + theta[2]*theta[2]);
  //assert(std::abs(angle)>1.e-4);
  double thetabar[3];
  double factor = -0.5*gsl_sf_sinc(angle/2.)/cos(angle/2.);
  for(int i=0; i<3; ++i)
    thetabar[i] = factor*theta[i];

  // Derivative of thetabar
  double val = (theta[0]*dtheta[0]+theta[1]*dtheta[1]+theta[2]*dtheta[2])/(angle*angle);
  double dthetabar[] = {val*theta[0], val*theta[1], val*theta[2]};
  val = 1./gsl_sf_sinc(angle)-1.;
  for(int i=0; i<3; ++i)
    {
      dthetabar[i] *= val;
      dthetabar[i] += dtheta[i];
    }
  val = 0.5*gsl_sf_sinc(0.5*angle)/cos(0.5*angle);
  for(int i=0; i<3; ++i)
    dthetabar[i] *= val;
  
  // Axial vector of (Exp[Theta])'Exp[-Theta]
  double beta[3];
  val = 2./(1.+(thetabar[0]*thetabar[0]+thetabar[1]*thetabar[1]+thetabar[2]*thetabar[2]));
  for(int i=0; i<3; ++i)
    beta[i] = val*(dthetabar[i] +
		   thetabar[(i+1)%3]*dthetabar[(i+2)%3]-thetabar[(i+2)%3]*dthetabar[(i+1)%3]);
  double Beta[3][3];
  HodgeStar(beta, Beta);

  // Omega_{n+1} = Beta + Exp[Theta] Omega_n Exp[-Theta]
  double mtheta[] = {-theta[0], -theta[1], -theta[2]};
  double mexp[3][3];
  ExpSO3(mtheta, mexp);
  double OmegaNew[3][3];
  for(int i=0; i<3; ++i)
    for(int j=0; j<3; ++j)
      {
	OmegaNew[i][j] = Beta[i][j];
	for(int k=0; k<3; ++k)
	  for(int L=0; L<3; ++L)
	    OmegaNew[i][j] += exp[i][k]*Omega[k][L]*mexp[L][j];
      }

  // Update kinematics at point
  for(int i=0; i<3; ++i)
    {
      phi[i] += eta[i];
      dphi[i] += deta[i];
      for(int j=0; j<3; ++j)
	{
	  Lambda[i][j] = LambdaNew[i][j];
	  Omega[i][j] = OmegaNew[i][j];
	}
    }
    

}


// Compute numerical values for first variations
void ComputeNumericalVariations(const PointKinematics& PD, const VarPointKinematics& var,
			       const double eps, double* varGamma, double* varKappa)
{
  // Scale variation
  VarPointKinematics varplus(var);
  VarPointKinematics varminus(var);
  for(int i=0; i<3; ++i)
    {
      varplus.eta[i] *= eps; varminus.eta[i] *= -eps;
      varplus.deta[i] *= eps; varminus.deta[i] *= -eps;
      varplus.theta[i] *= eps; varminus.theta[i] *= -eps;
      varplus.dtheta[i] *= eps; varminus.dtheta[i] *= -eps;
    }
  
  // Perturbed states
  PointKinematics PDplus(PD);
  Update(PDplus, varplus);
  PDplus.ComputeStrains();
  PointKinematics PDminus(PD);
  Update(PDminus, varminus);
  PDminus.ComputeStrains();
  
  // Numerical values of strain variations
  for(int i=0; i<3; ++i)
    {
      varGamma[i] = (PDplus.Gamma[i]-PDminus.Gamma[i])/(2.*eps);
      varKappa[i] = (PDplus.Kappa[i]-PDminus.Kappa[i])/(2.*eps);
    }
  return;
}


// Compute numerical values for second variations
void ComputeNumerical2ndVariations(const PointKinematics& PD,
				   const VarPointKinematics& var, const VarPointKinematics& VAR,
				   const double eps, double* vvGamma, double* vvKappa)
{
  // Update PD along the variation VAR
  VarPointKinematics VARplus(VAR), VARminus(VAR);
  for(int i=0; i<3; ++i)
    {
      VARplus.eta[i] *= eps; VARminus.eta[i] *= -eps;
      VARplus.deta[i] *= eps; VARminus.deta[i] *= -eps;
      VARplus.theta[i] *= eps; VARminus.theta[i] *= -eps;
      VARplus.dtheta[i] *= eps; VARminus.dtheta[i] *= -eps;
    }
  PointKinematics PDplus(PD);
  Update(PDplus, VARplus);
  PointKinematics PDminus(PD);
  Update(PDminus, VARminus);
  
  // Compute first variations with perturbed states
  VarPointKinematics varplus(var);
  varplus.PD = &PDplus;
  varplus.ComputeVarStrains();
  VarPointKinematics varminus(var);
  varminus.PD = &PDminus;
  varminus.ComputeVarStrains();

  // Compute second variations
  for(int i=0; i<3; ++i)
    {
      vvGamma[i] = (varplus.vGamma[i]-varminus.vGamma[i])/(2.*eps);
      vvKappa[i] = (varplus.vKappa[i]-varminus.vKappa[i])/(2.*eps);
    }
  return;
}

