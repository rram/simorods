// Sriramajayam

#include <Assembler.h>
#include <SimoRodsP11DElement.h>
#include <SimoRodsOps.h>
#include <SimoRodsUtils.h>
#include <random>
#include <cassert>
#include <iostream>

using namespace simorods;

// Random double 
std::random_device rd;  //Will be used to obtain a seed for the random number engine
std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
std::uniform_real_distribution<> dis(-1.,1.);

// Generate a random vector
Vec3 GenerateVector();

// Generate a random rotation
Mat3 GenerateRotation();

// Generate a random skew symm matrix
Mat3 GenerateSkew();

int main(int argc, char**argv)
{
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Reference configuration
  const int nNodes = 3;
  std::vector<double> Vertices({0., 0.23, 0.45});
  Segment<1>::SetGlobalCoordinatesArray(Vertices);

  // Create 2 elements
  const int nElements = 2;
  const int nFields = 6;
  const int nQuadsPerElm = 2;
  std::vector<Element*> ElmArray(nElements);
  for(int e=0; e<nElements; ++e)
    ElmArray[e] = new P11DElement<nFields>(e+1, e+2);
  
  // Local to global map
  StandardP11DMap L2GMap(ElmArray);
  
  // Create configuration
  Vec3 phi;
  Mat3 Lambda, Omega;
  Configuration Config(nNodes, nElements, nQuadsPerElm);
  for(int a=0; a<nNodes; ++a)
    {
      phi = GenerateVector();
      Config.SetCenterline(a, phi);
    }
  for(int e=0; e<nElements; ++e)
    for(int q=0; q<nQuadsPerElm; ++q)
      {
	Lambda = GenerateRotation();
	Config.SetRotation(e, q, Lambda);
	Omega = GenerateSkew();
	Config.SetVorticity(e, q, Omega);
      }
  Config.SetInitialized();

  // Create material
  MatConstants MC({2.+dis(gen), 1.25+dis(gen), 1.75+dis(gen), 1.5+dis(gen)});
  Material Mtl(MC);

  // Workspace for operations
  OpsWorkspace WrkSpc(nFields*ElmArray[0]->GetDof(0));

  // Create operations
  std::vector<Ops*> OpArray(nElements);
  const std::vector<int> Fields({0,1,2,3,4,5});
  for(int e=0; e<nElements; ++e)
    {
      // Access to configuration in this operation
      ConfigAccessStruct Str(std::vector<int>({e,e+1}), e, nQuadsPerElm);
      OpArray[e] = new Ops(ElmArray[e], Fields, Str, Mtl, &WrkSpc);
    }

  // Create assembler
  StandardAssembler<Ops> Asm(OpArray, L2GMap);
  
  // Create PETSc arrays
  PetscErrorCode ierr;
  const int dim = L2GMap.GetTotalNumDof();
  Vec resVec;
  ierr = VecCreate(PETSC_COMM_WORLD, &resVec); CHKERRQ(ierr);
  ierr = VecSetSizes(resVec,PETSC_DECIDE,dim);  CHKERRQ(ierr);
  ierr = VecSetFromOptions(resVec); CHKERRQ(ierr);
  Mat dresMat;
  ierr = MatCreateSeqDense(PETSC_COMM_SELF,dim,dim,PETSC_NULL,&dresMat); CHKERRQ(ierr);
  ierr = MatSetOption(dresMat,MAT_SYMMETRIC,PETSC_TRUE); CHKERRQ(ierr);

  // Assemble residue and stiffness
  Asm.Assemble(&Config, resVec, dresMat);

  // For computing perturbd residuals
  Vec presVec;
  Vec mresVec;
  ierr = VecDuplicate(resVec,&presVec); CHKERRQ(ierr);
  ierr = VecDuplicate(resVec,&mresVec); CHKERRQ(ierr);
  
  // Check consistency of residuals
  const double pertEPS = 1.e-5;
  const double tolEPS = 1.e-5;
  double* resvals;
  ierr = VecGetArray(resVec, &resvals); CHKERRQ(ierr);
  std::vector<double> dresvals(dim*dim);
  std::vector<int> indx(dim);
  for(int i=0; i<dim; ++i) indx[i] = i;
  ierr = MatGetValues(dresMat, dim, &indx[0], dim, &indx[0], &dresvals[0]); CHKERRQ(ierr);
  //ierr = MatGetArray(dresMat,&dresvals); CHKERRQ(ierr);

  for(int f=0; f<3; ++f)       // f -> centerline dofs
    for(int a=0; a<nNodes; ++a)
      {
	// Positive perturbation
	Configuration pConfig(Config);
	Vec3 eta;
	eta[0] = eta[1] = eta[2] = 0.;
	eta[f] = pertEPS;
	pConfig.UpdateCenterline(a, eta);
	pConfig.SetInitialized();

	// Negative perturbation
	Configuration mConfig(Config);
	eta[f] *= -1.;
	mConfig.UpdateCenterline(a, eta);
	mConfig.SetInitialized();

	// Perturbed energies
	double Eplus = 0., Eminus = 0.;
	for(int e=0; e<nElements; ++e)
	  {
	    Eplus += OpArray[e]->GetEnergy(&pConfig);
	    Eminus += OpArray[e]->GetEnergy(&mConfig);
	  }

	// Consistency of residuals
	double nres = (Eplus-Eminus)/(2.*pertEPS);
	assert(std::abs(resvals[nFields*a+f]-nres)<tolEPS && "Consistency of residuals failed.");

	// Consistency of stiffness
	Asm.Assemble(&pConfig, presVec);
	Asm.Assemble(&mConfig, mresVec);
	double *presvals, *mresvals;
	ierr = VecGetArray(presVec, &presvals); CHKERRQ(ierr);
	ierr = VecGetArray(mresVec, &mresvals); CHKERRQ(ierr);
	for(int g=0; g<nFields; ++g)
	  for(int m=0; m<nNodes; ++m)
	    {
	      double ndres = (presvals[nFields*m+g]-mresvals[nFields*m+g])/(2.*pertEPS);
	      int row = nFields*a+f;
	      int col = nFields*m+g;
	      double kval = dresvals[dim*row+col];
	      assert(std::abs(ndres-kval)<tolEPS && "Consistency of stiffness failed");
	    }
	ierr = VecRestoreArray(presVec, &presvals); CHKERRQ(ierr);
	ierr = VecRestoreArray(mresVec, &mresvals); CHKERRQ(ierr);
      }

  
  // Check for vorticity dofs- requires quadrature level updates in each element
  for(int n=0; n<nNodes; ++n)
    for(int f=0; f<3; ++f)
      {
	// This perturbation vector
	std::vector<double> pertVec(3*nNodes);
	std::fill(pertVec.begin(), pertVec.end(), 0.);
	pertVec[3*n+f] = pertEPS;
	
	// Positive & negative perturbed configurations
	Configuration pConfig(Config), mConfig(Config);
	for(int e=0; e<nElements; ++e)
	  {
	    const int nQuads = static_cast<int>(ElmArray[e]->GetIntegrationWeights(0).size());
	    for(int q=0; q<nQuads; ++q)
	      {
		// Perturbation at this quadrature point
		Vec3 theta, dtheta;
		theta[0] = theta[1] = theta[2] = 0.;
		dtheta[0] = dtheta[1] = dtheta[2] = 0.;
		for(int a=0; a<2; ++a)
		  {
		    // This global node number
		    const int nodenum = ElmArray[e]->GetElementGeometry().GetConnectivity()[a]-1;
		    for(int g=0; g<3; ++g)
		      {
			theta[g] += pertVec[3*nodenum+g]*ElmArray[e]->GetShape(g,q,a);
			dtheta[g] += pertVec[3*nodenum+g]*ElmArray[e]->GetDShape(g,q,a,0);
		      }
		  }
		
		// Update perturbed configurations at this quadrature point
		pConfig.UpdateRotations(e,q,theta,dtheta);
		for(int i=0; i<3; ++i)
		  { theta[i] *= -1.; dtheta[i] *= -1.; }
		mConfig.UpdateRotations(e,q,theta,dtheta);
	      }
	  }
	pConfig.SetInitialized();
	mConfig.SetInitialized();
	
	// Compute perturbed energies (for the whole structure)
	double Eplus = 0., Eminus = 0.;
	for(int e=0; e<nElements; ++e)
	  {
	    Eplus += OpArray[e]->GetEnergy(&pConfig);
	    Eminus += OpArray[e]->GetEnergy(&mConfig);
	  }

	// Consistency of residuals
	double nres = (Eplus-Eminus)/(2.*pertEPS);
	assert(std::abs(resvals[nFields*n+(f+3)]-nres)<tolEPS && "Consistency of residuals failed");
	
	// Consistency of stiffness
	Asm.Assemble(&pConfig, presVec);
	Asm.Assemble(&mConfig, mresVec);
	double *presvals, *mresvals;
	ierr = VecGetArray(presVec, &presvals); CHKERRQ(ierr);
	ierr = VecGetArray(mresVec, &mresvals); CHKERRQ(ierr);
	for(int m=0; m<nNodes; ++m)
	  for(int g=0; g<nFields; ++g)
	    {
	      double ndres = (presvals[nFields*m+g]-mresvals[nFields*m+g])/(2.*pertEPS);
	      int row = nFields*n+(f+3);
	      int col = nFields*m+g;
	      double kval = dresvals[dim*row+col];
	      std::cout<<"\nField f: "<<f<<", field g: "<<g<<", "<<kval<<" should be "<<ndres<<std::flush;
	      assert(std::abs(kval-ndres)<tolEPS && "Consistency of stiffness failed");
	    }
	ierr = VecRestoreArray(presVec, &presvals); CHKERRQ(ierr);
	ierr = VecRestoreArray(mresVec, &mresvals); CHKERRQ(ierr);
      }
  
  ierr = VecRestoreArray(resVec, &resvals); CHKERRQ(ierr);
  //ierr = MatRestoreArray(dresMat,&dresvals); CHKERRQ(ierr);
  
  // Clean up
  for(int e=0; e<nElements; ++e)
    {
      delete ElmArray[e];
      delete OpArray[e];
    }
  ierr = MatDestroy(&dresMat); CHKERRQ(ierr);
  ierr = VecDestroy(&resVec); CHKERRQ(ierr);
  ierr = VecDestroy(&presVec); CHKERRQ(ierr);
  ierr = VecDestroy(&mresVec); CHKERRQ(ierr);
  PetscFinalize(); 
}

// Generate a random vector
Vec3 GenerateVector()
{
  Vec3 vec;
  for(int i=0; i<3; ++i)
    vec[i] = dis(gen);
  return vec;
}

// Generate a random rotation
Mat3 GenerateRotation()
{
  // Generate a random rotation tensor using the axis-angle representation
  double theta = dis(gen);
  double e[3];
  while(true)
    {
      double norm = 0.;
      for(int i=0; i<3; ++i)
	{
	  e[i] = dis(gen);
	  norm +=e[i]*e[i];
	}
      norm = sqrt(norm);
      if(norm>1.e-2)
	{
	  for(int i=0; i<3; ++i)
	    e[i] /= norm;
	  break;
	}
    }

  // Create the rotation matrix
  double StarE[3][3];
  HodgeStar(e, StarE);
  double delta[3][3] = {{1.,0.,0.},{0.,1.,0.},{0.,0.,1.}};
  Mat3 R;
  for(int i=0; i<3; ++i)
    for(int j=0; j<3; ++j)
      R[i][j] =
	e[i]*e[j] + (delta[i][j]-e[i]*e[j])*cos(theta) + StarE[i][j]*sin(theta);
  return R;
}

// Generate a random skew symm matrix
Mat3 GenerateSkew()
{
  Vec3 vec = GenerateVector();
  double skw[3][3];
  HodgeStar(&vec[0], skw);
  Mat3 mat;
  for(int i=0; i<3; ++i)
    for(int j=0; j<3; ++j)
      mat[i][j] = skw[i][j];
  return mat;
}
   
