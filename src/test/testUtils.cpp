// Sriramajayam

#include <iostream>
#include <SimoRodsPointKinematics.h>
#include <SimoRodsUtils.h>
#include <random>

// Random double 
std::random_device rd;  //Will be used to obtain a seed for the random number engine
std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
std::uniform_real_distribution<> dis(-1.,1.);

using namespace simorods;

// Check reflexivity of axial vector and hodgestar operators
void CheckAxialHodgeStarReflexivity(const double* vec);

// Determinant of 3x3 matrix
double Det(const double Mat[][3]);

// Initialize values
void Initialize(PointKinematics& PD);

// Update state
void Update(PointKinematics& PD, const VarPointKinematics& var);

// Compute numerical values for first variations
void ComputeNumericalVariations(const PointKinematics& PD, const VarPointKinematics& var,
				const double EPS, double* varGamma, double* varKappa);

// Compute numerical values for second variations
void ComputeNumerical2ndVariations(const PointKinematics& PD,
				   const VarPointKinematics& var, const VarPointKinematics& VAR,
				   const double EPS, double* vvGamma, double* vvKappa);

int main()
{
  // Initialize point data
  PointKinematics PD;
  Initialize(PD);

  // Compute the strains
  PD.ComputeStrains();
  
  // Set up two independent variations
  VarPointKinematics varA;
  varA.PD = &PD;
  for(int i=0; i<3; ++i)
    { varA.eta[i] = dis(gen); varA.deta[i] = dis(gen);
      varA.theta[i] = dis(gen); varA.dtheta[i] = dis(gen); }
  varA.ComputeVarStrains();

  VarPointKinematics varB;
  varB.PD = &PD;
  for(int i=0; i<3; ++i)
    { varB.eta[i] = dis(gen); varB.deta[i] = dis(gen);
      varB.theta[i] = dis(gen); varB.dtheta[i] = dis(gen); }
  varB.ComputeVarStrains();

  // Check reflexivity of axial vector and hodgestar operators
  CheckAxialHodgeStarReflexivity(varA.theta);
  CheckAxialHodgeStarReflexivity(varB.theta);
  
  // Compute the first variations numerically
  const double EPS = 1.e-5;

  // Variation A
  double varAGamma[3], varAKappa[3];
  ComputeNumericalVariations(PD, varA, EPS, varAGamma, varAKappa);
  for(int i=0; i<3; ++i)
    assert(std::abs(varA.vGamma[i]-varAGamma[i])+std::abs(varA.vKappa[i]-varAKappa[i])<1.e-4);
  
  // Variation B
  double varBGamma[3], varBKappa[3];
  ComputeNumericalVariations(PD, varB, EPS, varBGamma, varBKappa);
  for(int i=0; i<3; ++i)
    assert(std::abs(varB.vGamma[i]-varBGamma[i])+std::abs(varB.vKappa[i]-varBKappa[i])<1.e-4);
  
  // Second variation
  VarVarPointKinematics varAB;
  varAB.PD = &PD;
  varAB.deltaPD = &varA;
  varAB.DELTAPD = &varB;
  varAB.ComputeVarVarStrains();

  // Check consistency of second variations
  double vvGamma[3], vvKappa[3];
  ComputeNumerical2ndVariations(PD, varA, varB, EPS, vvGamma, vvKappa);
  for(int i=0; i<3; ++i)
    assert(std::abs(varAB.vvGamma[i]-vvGamma[i])+std::abs(varAB.vvKappa[i]-vvKappa[i])<1.e-4);
  
}

// Check reflexivity of axial vector and hodgestar operators
void  CheckAxialHodgeStarReflexivity(const double* vec)
{
  double axial[3], Skw[3][3];
  HodgeStar(vec, Skw);
  AxialVector(Skw, axial);
  assert(std::abs(axial[0]-vec[0])+std::abs(axial[1]-vec[1])+std::abs(axial[2]-vec[2])<1.e-8);

  // Check that Skw is skew
  for(int i=0; i<3; ++i)
    for(int j=0; j<3; ++j)
      assert(std::abs(Skw[i][j]+Skw[j][i])<1.e-8);
}

// Determinant of 3x3 matrix
double Det(const double Mat[][3])
{
  double det = 0.;
  for(int i=0; i<3; ++i)
    det += Mat[0][i]*(Mat[1][(i+1)%3]*Mat[2][(i+2)%3]-Mat[1][(i+2)%3]*Mat[2][(i+1)%3]);
  return det;
}


// Initialize values
void Initialize(PointKinematics& PD)
{
  // Set random values for phi and phi'
  for(int i=0; i<3; ++i)
    {
      PD.phi[i] = dis(gen);
      PD.dphi[i] = dis(gen);
    }

  // Generate a random rotation tensor using the axis-angle representation
  double theta = dis(gen);
  double e[3];
  while(true)
    {
      double norm = 0.;
      for(int i=0; i<3; ++i)
	{
	  e[i] = dis(gen);
	  norm +=e[i]*e[i];
	}
      norm = sqrt(norm);
      if(norm>1.e-2)
	{
	  for(int i=0; i<3; ++i)
	    e[i] /= norm;
	  break;
	}
    }

  // Create the rotation matrix
  double StarE[3][3];
  HodgeStar(e, StarE);
  double delta[3][3] = {{1.,0.,0.},{0.,1.,0.},{0.,0.,1.}};
  for(int i=0; i<3; ++i)
    for(int j=0; j<3; ++j)
      PD.Lambda[i][j] =
	e[i]*e[j] + (delta[i][j]-e[i]*e[j])*cos(theta) + StarE[i][j]*sin(theta);
  
  // Check that the rotation matrix has determinant 1
  assert(std::abs(Det(PD.Lambda)-1.)<1.e-8);
  
  // Create the Omega matrix
  double omega[] = {dis(gen)/3., dis(gen)/3., dis(gen)/3.};
  HodgeStar(omega, PD.Omega);
  assert(std::abs(Det(PD.Omega)-0.)<1.e-8);
}


// Update state
void Update(PointKinematics& PD, const VarPointKinematics& var)
{
  // Aliases
  auto* phi = PD.phi;
  auto* dphi = PD.dphi;
  auto* Lambda = PD.Lambda;
  auto* Omega = PD.Omega;
  
  const auto* eta = var.eta;
  const auto* deta = var.deta;
  const auto* theta = var.theta;
  const auto* dtheta = var.dtheta;

  // Update rotations Lambda = exp(Skw(theta)) Lambda
  double exp[3][3];
  ExpSO3(theta, exp);  assert(std::abs(Det(exp)-1.)<1.e-7);
  double LambdaNew[3][3];
  for(int i=0; i<3; ++i)
    for(int j=0; j<3; ++j)
      {
	LambdaNew[i][j] = 0.;
	for(int k=0; k<3; ++k)
	  LambdaNew[i][j] += exp[i][k]*Lambda[k][j];
      }
  assert(std::abs(Det(LambdaNew)-1.)<1.e-7);

  // Compute (Exp[Theta])'Exp[-Theta]
  double dexp[3][3];
  dExpSO3Proj(theta, dtheta, dexp); assert(std::abs(Det(dexp)-0.)<1.e-7);
  
  // Omega_{n+1} = (Exp[Theta])'Exp[-Theta] + Exp[Theta] Omega_n Exp[-Theta]
  double mtheta[] = {-theta[0], -theta[1], -theta[2]};
  double mexp[3][3];
  ExpSO3(mtheta, mexp); assert(std::abs(Det(mexp)-1.)<1.e-7);
  double OmegaNew[3][3];
  for(int i=0; i<3; ++i)
    for(int j=0; j<3; ++j)
      {
	OmegaNew[i][j] = dexp[i][j];
	for(int k=0; k<3; ++k)
	  for(int L=0; L<3; ++L)
	    OmegaNew[i][j] += exp[i][k]*Omega[k][L]*mexp[L][j];
      }
  assert(std::abs(Det(OmegaNew)-0.)<1.e-7);

  // Update kinematics at point
  for(int i=0; i<3; ++i)
    {
      phi[i] += eta[i];
      dphi[i] += deta[i];
      for(int j=0; j<3; ++j)
	{
	  Lambda[i][j] = LambdaNew[i][j];
	  Omega[i][j] = OmegaNew[i][j];
	}
    }
    

}


// Compute numerical values for first variations
void ComputeNumericalVariations(const PointKinematics& PD, const VarPointKinematics& var,
				const double eps, double* varGamma, double* varKappa)
{
  // Scale variation
  VarPointKinematics varplus(var);
  VarPointKinematics varminus(var);
  for(int i=0; i<3; ++i)
    {
      varplus.eta[i] *= eps; varminus.eta[i] *= -eps;
      varplus.deta[i] *= eps; varminus.deta[i] *= -eps;
      varplus.theta[i] *= eps; varminus.theta[i] *= -eps;
      varplus.dtheta[i] *= eps; varminus.dtheta[i] *= -eps;
    }
  
  // Perturbed states
  PointKinematics PDplus(PD);
  Update(PDplus, varplus);
  PDplus.ComputeStrains();
  PointKinematics PDminus(PD);
  Update(PDminus, varminus);
  PDminus.ComputeStrains();
  
  // Numerical values of strain variations
  for(int i=0; i<3; ++i)
    {
      varGamma[i] = (PDplus.Gamma[i]-PDminus.Gamma[i])/(2.*eps);
      varKappa[i] = (PDplus.Kappa[i]-PDminus.Kappa[i])/(2.*eps);
    }
  return;
}


// Compute numerical values for second variations
void ComputeNumerical2ndVariations(const PointKinematics& PD,
				   const VarPointKinematics& var, const VarPointKinematics& VAR,
				   const double eps, double* vvGamma, double* vvKappa)
{
  // Update PD along the variation VAR
  VarPointKinematics VARplus(VAR), VARminus(VAR);
  for(int i=0; i<3; ++i)
    {
      VARplus.eta[i] *= eps; VARminus.eta[i] *= -eps;
      VARplus.deta[i] *= eps; VARminus.deta[i] *= -eps;
      VARplus.theta[i] *= eps; VARminus.theta[i] *= -eps;
      VARplus.dtheta[i] *= eps; VARminus.dtheta[i] *= -eps;
    }
  PointKinematics PDplus(PD);
  Update(PDplus, VARplus);
  PointKinematics PDminus(PD);
  Update(PDminus, VARminus);
  
  // Compute first variations with perturbed states
  VarPointKinematics varplus(var);
  varplus.PD = &PDplus;
  varplus.ComputeVarStrains();
  VarPointKinematics varminus(var);
  varminus.PD = &PDminus;
  varminus.ComputeVarStrains();

  // Compute second variations
  for(int i=0; i<3; ++i)
    {
      vvGamma[i] = (varplus.vGamma[i]-varminus.vGamma[i])/(2.*eps);
      vvKappa[i] = (varplus.vKappa[i]-varminus.vKappa[i])/(2.*eps);
    }
  return;
}

