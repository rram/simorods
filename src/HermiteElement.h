// Sriramajayam

#ifndef HERMITE_ELEMENT_H
#define HERMITE_ELEMENT_H

#include <Element.h>
#include <HermiteShape.h>
#include <Segment.h>
#include <ShapesEvaluated.h>
#include <LineQuadratures.h>

template<int nFields>
class HermiteElement: public Element
{
 public:
  //! Constructor
  //! \param[in] i1,i2 Node numbers
  HermiteElement(const int i1, const int i2);

  //! Destructor
  inline virtual ~HermiteElement()
  { delete SegGeom; }

  //! Copy constructor
  //! \param[in] obj Object to be copied from
  HermiteElement(const HermiteElement<nFields>& obj)
    :Element(obj),
    SegGeom(obj.SegGeom->Clone()),
    D2Shapes(obj.D2Shapes) {}

  //! Cloning
  inline virtual HermiteElement<nFields>* Clone() const override
  { return new HermiteElement<nFields>(*this); }

  //! Access element geometry
  const Segment<1>& GetElementGeometry() const override
  { return *SegGeom; }

  //! Access 2nd derivatives
  //! \param[in] field Field number
  //! \param[in] q Quadrature point number
  //! \param[in] a Shape function number
  inline double GetD2Shape(const int field, const int q, const int a) const
  { assert(a>=0 && a<4 && q>=0 && q<8);
    return D2Shapes[4*q+a]; }
  
 protected:
  //! Return the position in LocalShapes where shape functions for a 
  //! given field are saved.
  //! \param fieldnum Field number
  inline virtual int GetFieldIndex(int fieldnum) const override
  { return 0; }

 private:
  Segment<1>* SegGeom; //!< Element geoemtry
  std::vector<double> D2Shapes; //!< 2nd derivatives. D2Shapes[4*q+a] = Na''(Xq).
};


// Implement constructor
template<int nFields> HermiteElement<nFields>::
HermiteElement(const int i1, const int i2)
:Element()
{
  // Create the segment geometry
  SegGeom = new Segment<1>(i1, i2);

  // Shape functions
  double endpts[2];
  double lambda;
  lambda = 1.; SegGeom->Map(&lambda, &endpts[0]); // Left end point
  lambda = 0.; SegGeom->Map(&lambda, &endpts[1]); // Right end point
  HermiteShape Shp(endpts);

  // 8 point quadrature
  const Quadrature* Qrule = Line_8pt::Bulk;

  // Evaluate shape functions and derivatives
  ShapesEvaluated ShpEval(Qrule, &Shp, SegGeom);
  AddBasisFunctions(ShpEval);
  for(int i=0; i<nFields; ++i)
    AppendField(i);

  // Evaluate 2nd derivatives of shape functions
  const int nQuad = Qrule->GetNumberQuadraturePoints();
  const int nShapes = Shp.GetNumberOfFunctions();
  D2Shapes.resize(nShapes*nQuad);
  const double Jac2 = (endpts[1]-endpts[0])*(endpts[1]-endpts[0]);
  for(int q=0; q<nQuad; ++q)
    {
      // This quadrature point (lambda)
      const double* qlambda = Qrule->GetQuadraturePoint(q);
      // 2nd derivatives of shape functions
      for(int a=0; a<nShapes; ++a)
	{
	  //d2Na/dx^2 = d^2Na/dlambda^2 /Jac^2
	  D2Shapes[q*nShapes+a] = Shp.D2Val(a,qlambda)/Jac2;
	}
    }
  // done	    
}


#endif
