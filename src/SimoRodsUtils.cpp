// Sriramajayam

#include <SimoRodsUtils.h>
#include <cmath>
#include <gsl/gsl_sf_bessel.h>

// Utility functions
namespace simorods
{
  // Computes the axial vector of a 3x3 skew symmetric matrix
  void AxialVector(const double skw[][3], double* theta)
  {
    theta[0] = skw[2][1];
    theta[1] = skw[0][2];
    theta[2] = skw[1][0]; 
    return;
  }

  // Computes the skew symmetric matrix for a given axial vector
  void HodgeStar(const double* theta, double skw[][3])
  {
    skw[0][0] = 0.;        skw[0][1] = -theta[2]; skw[0][2] = theta[1];
    skw[1][0] = theta[2];  skw[1][1] = 0.;        skw[1][2] = -theta[0];
    skw[2][0] = -theta[1]; skw[2][1] = theta[0];  skw[2][2] = 0.;
    return;
  }


  // Computes the exponential map given an axial vector
  void ExpSO3(const double* vec, double Mat[][3])
  {
    double Skw[3][3];
    HodgeStar(vec, Skw);
    ExpSO3(Skw, Mat);
    return;
  }

  
  // Computes the exponential map given a skew symm matrix
  void ExpSO3(const double skw[][3], double Mat[][3])
  {
    // I + sinc(x) Skw + (1-cos(x))/x^2 Skw^2
    // = I + sinc(x) skw + (1/2) sinc(x/2)^2 skw^2
    
    // Angle
    double angle = sqrt(skw[2][1]*skw[2][1] +
			skw[0][2]*skw[0][2] +
			skw[1][0]*skw[1][0]);
    
    double sinc = gsl_sf_bessel_j0(angle); // sin(x)/x
    double sincby2sq = gsl_sf_bessel_j0(angle/2.);  // sin(x/2)/(x/2)
    sincby2sq *= sincby2sq;
    
    for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
	{
	  Mat[i][j] = 0.;
	  for(int k=0; k<3; ++k)
	    Mat[i][j] += skw[i][k]*skw[k][j];
	  Mat[i][j] *= 0.5*sincby2sq;
	  Mat[i][j] += sinc*skw[i][j];
	}
    Mat[0][0] += 1.;
    Mat[1][1] += 1.;
    Mat[2][2] += 1.;
  }
  
  // Computes the projected derivative of the exponential map
  // [d/dS exp(theta(S))] exp(-theta(s))
  void dExpSO3Proj(const double* theta, const double* dtheta,
		   double Mat[][3])
  {
    // thetabar = (1/2) tan(x/2) theta
    double angle = sqrt(theta[0]*theta[0]+theta[1]*theta[1]+theta[2]*theta[2]);
    double tanc = 0.5*gsl_sf_bessel_j0(angle/2.)/cos(angle/2.);
    double thetabar[] = {theta[0]*tanc, theta[1]*tanc, theta[2]*tanc};
    
    // (theta.dtheta)/(theta.theta)(x/sin(x)-1)
    double factor = (theta[0]*dtheta[0]+theta[1]*dtheta[1]+theta[2]*dtheta[2]);
    if(std::abs(angle)<1.e-6)
      factor *= 1./6.;
    else
      factor *= (1./gsl_sf_bessel_j0(angle)-1.)/(angle*angle);

    // d(thetabar)
    double dthetabar[3];
    for(int i=0; i<3; ++i)
      dthetabar[i] = tanc*(dtheta[i]+factor*theta[i]);
    
    // Axial vector of (Exp[Theta])'Exp[-Theta]
    double beta[3];
    factor = 2./(1.+(thetabar[0]*thetabar[0]+thetabar[1]*thetabar[1]+thetabar[2]*thetabar[2]));
    for(int i=0; i<3; ++i)
      beta[i] = factor*
	(dthetabar[i] +
	 thetabar[(i+1)%3]*dthetabar[(i+2)%3]-thetabar[(i+2)%3]*dthetabar[(i+1)%3]);
    
    // Return the matrix derivative
    HodgeStar(beta, Mat);
    return;
  }
  
}

