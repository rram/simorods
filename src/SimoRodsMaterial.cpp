// Sriramajayam

#include <SimoRodsMaterial.h>

namespace simorods
{
  // Compute the energy density
  double Material::
  ComputeStrainEnergyDensity(const double* Gamma, const double* Kappa) const
  {
    return 0.5*
      ( props.GA1*Gamma[0]*Gamma[0] +
	props.GA2*Gamma[1]*Gamma[1] +
	props.EA*Gamma[2]*Gamma[2] +
	props.EI1*Kappa[0]*Kappa[0] +
	props.EI2*Kappa[1]*Kappa[1] +
	props.GJ*(Kappa[2]*Kappa[2]) ); 
  }
  
  // Compute the stress and moment resultants
  void Material::
  ComputeResultants(const double* Gamma, const double* Kappa,
		    double* N, double* M) const
  {
    N[0] = props.GA1*Gamma[0];
    N[1] = props.GA2*Gamma[1];
    N[2] = props.EA*Gamma[2];
    M[0] = props.EI1*Kappa[0];
    M[1] = props.EI2*Kappa[1];
    M[2] = props.GJ*Kappa[2];
  }

  
  // Returns the first variation of the stress/moment resultants
  void Material::
  ComputeVarResultants(const double* Gamma, const double* Kappa,
		       const double* vGamma, const double* vKappa,
		       double* vN, double* vM) const
  {
    vN[0] = props.GA1*vGamma[0];
    vN[1] = props.GA2*vGamma[1];
    vN[2] = props.EA*vGamma[2];
    vM[0] = props.EI1*vKappa[0];
    vM[1] = props.EI2*vKappa[1];
    vM[2] = props.GJ*vKappa[2];
  }
      
  // Returns the material tangents
  void Material::
  ComputeMaterialTangents(const double* Gamma, const double* Kappa,
			  double Cgg[][3], double Cgk[][3], double Ckk[][3]) const
  {
    for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
	{
	  Cgg[i][j] = 0.;
	  Cgk[i][j] = 0.;
	  Ckk[i][j] = 0.;
	}
    Cgg[0][0] = props.GA1; Cgg[1][1] = props.GA2; Cgg[2][2] = props.EA;
    Ckk[0][0] = props.EI1; Ckk[1][1] = props.EI2; Ckk[2][2] = props.GJ;
  }
  
}
