// Sriramajayam

#ifndef SIMO_ROTS_MATERIAL_H
#define SIMO_ROTS_MATERIAL_H

#include <cassert>
#include <SimoRodsPointKinematics.h>

namespace simorods
{
  //! Encapsulate material constants
  class MatConstants
  {
  public:
    double GA1, GA2; //!< Shear stiffness
    double EA; //!< Axial stiffness
    double EI1, EI2; //!< Bending stiffness
    double GJ; //!< Torsional stiffness

    //! No default constructor
    inline MatConstants() = delete;

    //! Constructor
    inline MatConstants(double ga, double ea, double ei, double gj)
      :GA1(ga), GA2(ga), EA(ea), EI1(ei), EI2(ei), GJ(gj) {}
    
    //! Constructor
    inline MatConstants(double ga1, double ga2, double ea, double ei1, double ei2, double gj)
      :GA1(ga1), GA2(ga2), EA(ea), EI1(ei1), EI2(ei2), GJ(gj) {}
 
    //! Copy constructor
    inline MatConstants(const MatConstants& Obj)
      :GA1(Obj.GA1), GA2(Obj.GA2), EA(Obj.EA), EI1(Obj.EI1), EI2(Obj.EI2), GJ(Obj.GJ) {}

    //! Destructor
    inline virtual ~MatConstants() {}

    //! Assignment operator
    inline MatConstants& operator=(const MatConstants& rhs)
      {
	if(&rhs==this) return *this;
	this->GA1 = rhs.GA1; this->GA2 = rhs.GA2;
	this->EA = rhs.EA;
	this->EI1 = rhs.EI1; this->EI2 = rhs.EI2;
	this->GJ = rhs.GJ;
	return *this;
      }
  };


  
  //! Material for a rod in Simo's geometrically exact model
  class Material
  {
  public:
    //! Constructor with provided material constants
    //! \param[in] mc Material constants, copied
    inline Material(const MatConstants mc)
      :props(mc) {}

    //! Destructor, does nothing
    inline virtual ~Material() {}

    //! Copy constructor
    inline Material(const Material& Obj)
      :props(Obj.props) {}

    //! Disable assignment
    Material& operator=(const Material) = delete;
    
    //! Returns the set of material properties
    inline MatConstants GetMaterialConstants() const
    { return props; }

    //! Set material constants
    inline void SetMaterialConstants(const MatConstants& mc)
    {
      props.EA = mc.EA;   props.GA1 = mc.GA1; props.GA2 = mc.GA2;
      props.EI1 = mc.EI1; props.EI2 = mc.EI2; props.GJ = mc.GJ;
      return;
    }
    
    //! Compute the energy density
    //! \param[in] Gamma, Kappa Strain measures
    double ComputeStrainEnergyDensity(const double* Gamma, const double* Kappa) const;

    //! Compute the stress and moment resultants
    //! \param[in] Gamma, Kappa Strain measures
    //! \param[out] N, M Stress and moment resultants
    void ComputeResultants(const double* Gamma, const double* Kappa,
			   double* N, double* M) const;


    //! Returns the first variation of the stress/moment resultants
    //! \param[in] Gamma, Kappa Strain measures
    //! \param[in] vGamma, vKappa Variation of strain measures
    //! \param[out] vN, vM Variations of stress/moment resultants
    void ComputeVarResultants(const double* Gamma, const double* Kappa,
			      const double* vGamma, const double* vKappa,
			      double* vN, double* vM) const;
      
    //! Returns the material tangents
    //! \param[in] Gamma, Kappa Strain measures
    //! \param[in] Cgg, Cgk, Ckk Material tangents
    void ComputeMaterialTangents(const double* Gamma, const double* Kappa,
				 double Cgg[][3], double Cgk[][3], double Ckk[][3]) const;

  private:
    MatConstants props;
  };
  
}
#endif
