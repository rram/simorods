// Sriramajayam

#ifndef SIMO_RODS_OPS_WORKSPACE_H
#define SIMO_RODS_OPS_WORKSPACE_H

#include <vector>

namespace simorods
{
  //! Struct for each Ops object to encapsulate
  //! how to access its configuration from a Configuration object
  struct ConfigAccessStruct
  {
    std::vector<int> nodes; //!< Node number for centerline dofs
    int elm; //!< Element number
    int nQuadsPerElm; //!< Number of quadrature points per element
    //! Constructor
    inline ConfigAccessStruct(const std::vector<int> n, const int e, const int nQuads)
      :nodes(n), elm(e), nQuadsPerElm(nQuads) {}
    //! Copy constructor
    inline ConfigAccessStruct(const ConfigAccessStruct& str)
      :nodes(str.nodes), elm(str.elm), nQuadsPerElm(str.nQuadsPerElm) {}
    //! Assignment operator
    inline ConfigAccessStruct& operator=(const ConfigAccessStruct& rhs)
    {
      if(this==&rhs)
	return *this;
      elm = rhs.elm;
      nQuadsPerElm = rhs.nQuadsPerElm;
      nodes.assign(rhs.nodes.begin(), rhs.nodes.end());
      return *this;
    }
  };


  //! Workspace for intermediate calculations required in Ops
  //! \todo Make this thread-safe
  struct OpsWorkspace
  {
  private:
    const int nFields; //!< Number of fields
    const int nDofs;   //!< Number of dofs per field
    
  public:
    PointKinematics PD; //!< Kinematic data at a quadrature point
    std::vector<std::vector<VarPointKinematics>> varPD; //!< First variations
    VarVarPointKinematics varvarPD; //!< Second variations
    double tempval; //!< Intermediate variable
    
  public:
    //! Constructor
    //! \param[in] nvars max. number of first variations- can be an estimate
    inline OpsWorkspace(const int nfields, const int ndofs)
      :nFields(nfields), nDofs(ndofs), 
      varPD(nFields, std::vector<VarPointKinematics>(nDofs)) {}
    
    //! Destructor, does nothing
    inline virtual ~OpsWorkspace() {}

    //! Copy constructor
    //! \param[in] Obj Object to be copied. Only used for sizing
    inline OpsWorkspace(const OpsWorkspace& Obj)
      :nFields(Obj.nFields), nDofs(Obj.nDofs), 
      varPD(Obj.nFields, std::vector<VarPointKinematics>(Obj.nDofs)) {}
    
    //! Disable assignment
    OpsWorkspace& operator=(const OpsWorkspace&) = delete;
  };

}

#endif
