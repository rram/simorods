// Sriramajayam

#ifndef SIMO_RODS_CONFIGURATION_H
#define SIMO_RODS_CONFIGURATION_H

#include <vector>
#include <array>

namespace simorods
{
  //! Class defining the configuration of a rod
  using Vec3 = std::array<double,3>;
  using Mat3 = std::array<std::array<double,3>,3>;

  //! The state of a rod has to be initialized explicitly before the
  //! first time it is used, as well as after each time it is updated.
  //! Assumes the same number of quadrature points in each element
  class Configuration
  {
  public:
    //! Constructor
    //! \param[in] nnodes Number of nodes in the rod
    //! \param[in] nelms Number of elements in the rod
    //! \param[in] nquadsperelm Number of quadrature points per element
    Configuration(int nnodes, int nelms, int nquadsperelm)
      :nNodes(nnodes), nElms(nelms), nQuadsPerElm(nquadsperelm),
      Phi(nNodes), Lambda(nElms*nQuadsPerElm), Omega(nElms*nQuadsPerElm),
      isInitialized(false) {}
    
    //! Destructor, does nothing
    inline virtual ~Configuration() {}

    //! Copy constuctor
    Configuration(const Configuration& Obj);
    
    //! Disable assignment operator
    Configuration& operator=(const Configuration&) = delete;
    
    //! Mark the state of the rod as initialized
    inline void SetInitialized()
    { isInitialized = true; }

    //! Returns if the configuration has been set or not
    inline bool IsInitialized() const
    { return isInitialized; }


    //! Set the values of the centerline coordinates
    //! \param[in] a Node number
    //! \param[in] val Phi vector at the node
    void SetCenterline(const int a, Vec3 val);

    //! Set the values of the rotations
    //! \param[in] elm Element number
    //! \param[in] q Quadrature point in the element
    //! \param[in] val Rotation tensor
    void SetRotation(const int elm, const int q, Mat3 val);

    //! Set the values of the vorticity
    //! \param[in] elm Element number
    //! \param[in] q Quadrature point in the element
    //! \param[in] val Vorticity tensor
    void SetVorticity(const int elm, const int q, Mat3 val);

    //! Returns the number of nodes
    inline int GetNumNodes() const
    { return nNodes; }

    //! Returns the number of elements
    inline int GetNumElements() const
    { return nElms; }

    //! Returns the number of quadrature points/element
    inline int GetNumQuadraturePointsPerElement() const
    { return nQuadsPerElm; }
    
    //! Returns the centerline coordinates
    const std::vector<Vec3>& GetCenterline() const;

    //! Returns the centerline coordinates
    //! \param[in] a Node number
    const Vec3& GetCenterline(const int a) const;

    //! Returns the rotation tensor
    const std::vector<Mat3>& GetRotations() const;

    //! Returns the rotation tensor
    //! \param[in] elm Element number
    //! \param[in] q Quadrature point number in the element
    const Mat3& GetRotations(const int elm, const int q) const;

    //! Returns the vorticity tensor
    const std::vector<Mat3>& GetVorticity() const;

    //! Returns the vorticity tensor
    //! \param[in] elm Element number
    //! \param[in] q Quadrature point number in the element
    const Mat3& GetVorticity(const int elm, const int q) const;

    //! Updates the centerline coordinates
    //! \param[in] a Node number
    //! \param[in] incPhi Increment in centerline coordinates
    void UpdateCenterline(const int a, const Vec3& incPhi);

    //! Update the centerline coordinates
    //! \param[in] incPhi Increment in centerline coordinates
    void UpdateCenterline(const std::vector<Vec3>& incPhi);

    //! Update the rotations
    //! \param[in] elm Element number
    //! \param[in] q Quadrature point number
    //! \param[in] incTheta, incdThtea Increments in vorticity, derivatives
    void UpdateRotations(const int elm, const int q,
			 const Vec3& incTheta, const Vec3& incdTheta);

    //! Update the rotations
    //! \param[in] incTheta, incdThtea Increments in vorticity, derivatives
    void UpdateRotations(const std::vector<Vec3>& incTheta,
			 const std::vector<Vec3>& incdTheta);

  private:
    const int nNodes, nElms, nQuadsPerElm; //!< Number of nodes, elements and qpts per element.
    std::vector<Vec3> Phi; //!< Coordinates of the centerline at the nodes
    std::vector<Mat3> Lambda; //!< Rotation tensor at the quadrature points
    std::vector<Mat3> Omega; //!< Vortivity of rotation tensor at the quadrature points
    bool isInitialized; //!< has the initial configuration of the rod initialized
  };
}

#endif
