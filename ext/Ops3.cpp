// Sriramajayam

#include <Ops3.h>
#include <NewPointKinematics.h>
#include <SimoRodsUtils.h>
#include <cmath>
#include <cassert>
#include <iostream>

namespace simorods
{
  // Energy calculation
  double Ops3::GetEnergy(const void* arg) const
  {
    const int SPD = 3;
    
    // Cast configuration
    assert(arg!=nullptr);
    const BiRodConfiguration* Config = static_cast<const BiRodConfiguration*>(arg); 
    assert(Config!=nullptr && "simorods::Ops3::GetEnergy- Invalid pointer to configration13"); 

    // Check that this configuration is "set"
    assert(Config->IsInitialized() &&
	   "Ops3::GetEnergy()- Configuration not initialized");
    
    // Get the rods and their separation distance
    const auto& Rod1 = Config->GetRod1();
    const auto& Rod2 = Config->GetRod2();
    const double ConstraintDist2 = Config->GetConstraintDistance2();
    
    // Energy to be computed
    double Energy = 0.;

    // Access workspace for quadrature point-level calculatons
    auto& PD = WrkSpc->PD;
    
    // Number of quadrature points
    const auto& nQuads = ConfigStr.nQuadsPerElm;
    assert(nQuads==static_cast<int>(Elm->GetIntegrationWeights(fields[0]).size()));
    
    // Node number for this element
    const auto& nodes = ConfigStr.nodes;
    assert(static_cast<int>(nodes.size())==Elm->GetDof(fields[0]));
    const int ndofs = static_cast<int>(nodes.size());
    
    // Quadrature weights
    const auto& Qwts = Elm->GetIntegrationWeights(fields[0]);

    // Integrate
    double dist2;
    for(int q=0; q<nQuads; ++q)
      {
	// Centerline coordinates at this point for rod1 and rod2
	for(int f=0; f<SPD; ++f)
	  {
	    PD.phi1[f] = 0.;
	    PD.phi2[f] = 0.;
	    for(int a=0; a<ndofs; ++a)
	      {
		const auto& x1 = Rod1.GetCenterline(nodes[a]);
		const auto& x2 = Rod2.GetCenterline(nodes[a]);
		PD.phi1[f] += x1[f]*Elm->GetShape(fields[f],q,a);
		PD.phi2[f] += x2[f]*Elm->GetShape(fields[f+SPD],q,a);
	      }
	  }

	// (phi1 - phi2).(phi1 - phi2)
	dist2 = 0.;
	for(int i=0; i<SPD; ++i)
	  dist2 += (PD.phi1[i] - PD.phi2[i])*(PD.phi1[i] - PD.phi2[i]);

	// Update the energy
	// K/4 ((phi1 - phi2).(phi1 - phi2) - c)^2
	Energy += 0.25*Qwts[q]*PenaltyParam*(dist2-ConstraintDist2)*(dist2-ConstraintDist2);
      }
    
    return Energy;
  }

  
  // Residual calculation
  void Ops3::GetVal(const void* arg, 
		    std::vector<std::vector<double>>* resval) const
  { GetDVal(arg, resval, nullptr); }

  
  // Residual and stiffness calculation
  void Ops3::GetDVal(const void* arg, 
		     std::vector<std::vector<double>>* resval,
		     std::vector<std::vector<std::vector<std::vector<double>>>>* dresval) const
  {
    const int SPD = 3;
    const int nFields = static_cast<int>(fields.size());
    
    // Cast configuration variable
    assert(arg!=nullptr);
    const BiRodConfiguration* Config = static_cast<const BiRodConfiguration*>(arg);
    assert(Config!=nullptr && "simorods::Ops3::GetDVal- Invalid configuration pointer");
    
    // Check that this provided configuration has been set
    assert(Config->IsInitialized() &&
	   "simorods::Ops3::GetDResidue()- Configuration not initialized.");

    // Get the rods and constraint distances
    const auto& Rod1 = Config->GetRod1();
    const auto& Rod2 = Config->GetRod2();
    const double cdist2 = Config->GetConstraintDistance2();
    
    // initialize residual and stiffness to zero
    SetZero(resval, dresval);
    
    // Assume that at least one of the residual/stiffness is required
    assert((resval!=nullptr || dresval!=nullptr) &&
	   "simorods::Ops3::GetDResidue()- Residual is assumed to be required");
    
    // Access the workspace 
    auto& PD = WrkSpc->PD;
    auto& varPD = WrkSpc->varPD;
    
    // Number of quadrature points
    const auto& nQuad = ConfigStr.nQuadsPerElm; // ConfigStr is coming from Configuration13
    
    // Quadrature weights
    const auto& Qwts = Elm->GetIntegrationWeights(fields[0]);
    assert(static_cast<int>(Qwts.size())==nQuad);
    
    // Access details for configuration
    const auto& nodes = ConfigStr.nodes;
    const int ndofs = static_cast<int>(nodes.size());
    assert(ndofs==Elm->GetDof(fields[0]) && "simorods::Ops3::GetDResidue- Inconsistent number of dofs");
    
    // Integrate
    double tempval;
    double tempval1, tempval2, tempval3;
    double dist2;
    for(int q=0; q<nQuad; ++q)
      {
	// Set the configuration at this point, Centerlines of both the rods
	for(int f=0; f<SPD; ++f)
	  {
	    PD.phi1[f] = 0.;
	    PD.phi2[f] = 0.;
	    for(int a=0; a<ndofs; ++a)
	      {
		const auto& x1 = Rod1.GetCenterline(nodes[a]);
		PD.phi1[f] += x1[f]*Elm->GetShape(fields[f],q,a);
		const auto& x2 = Rod2.GetCenterline(nodes[a]);
		PD.phi2[f] += x2[f]*Elm->GetShape(fields[f+SPD],q,a);
	      }
	  }

	// Distance constraint
	dist2 = 0.;
	for(int i=0; i<SPD; ++i)
	  dist2 += (PD.phi1[i]-PD.phi2[i])*(PD.phi1[i]-PD.phi2[i]);
	
	// Set centerline  variations for both the rods
	// eta1 is the variation of the centerline of rod1
	// eta2 is the  variation of the centerline of rod2
	for(int f=0; f<SPD; ++f)
	  for(int a=0; a<ndofs; ++a)
	    {
	      auto& var = varPD[f][a];
	      var.SetZero();
	      var.PD = &PD; 
	      var.eta1[f] = Elm->GetShape(fields[f],q,a);
	      
	      auto& VAR = varPD[f+SPD][a];
	      VAR.SetZero(); 
	      VAR.PD = &PD; 
	      VAR.eta2[f] = Elm->GetShape(fields[f+SPD],q,a);
	    }
	
	// Compute the residue
	if(resval!=nullptr)
	  for(int f=0; f<nFields; ++f)
	    for(int a = 0; a<ndofs; ++a)
	      {
		auto& var = varPD[f][a];
		tempval = 0.;
		for(int i=0; i<3; ++i)
		  tempval += (var.eta1[i] - var.eta2[i])*(PD.phi1[i]- PD.phi2[i]);

		// K (dist2-cdist2)*(eta1.(phi1 - phi2) + eta2.(phi2 - phi1))
		(*resval)[fields[f]][a] += Qwts[q]*PenaltyParam*(dist2-cdist2)*tempval;
	      }
	
	
	// Compute the stiffness
	if(dresval!=nullptr)
	  for(int f=0; f<nFields; ++f)
	    for(int a=0; a<ndofs; ++a)
	      {
		const auto& var = varPD[f][a];

		tempval1 = 0.;
		for(int i=0; i<SPD; ++i)
		  tempval1 += (var.eta1[i] - var.eta2[i])*(PD.phi1[i]- PD.phi2[i]);
		
		for(int g=0; g<nFields; ++g)
		  for(int b=0; b<ndofs; ++b)
		    {
		      const auto& VAR = varPD[g][b];

		      	tempval2 = 0.;
			for(int i=0; i<SPD; ++i)
			  tempval2 += (VAR.eta1[i] - VAR.eta2[i])*(PD.phi1[i]- PD.phi2[i]);

			tempval3 = 0.;
			for(int i=0; i<SPD; ++i)
			  tempval3 += (var.eta1[i] - var.eta2[i])*(VAR.eta1[i]- VAR.eta2[i]);

			// Update the stiffness
			(*dresval)[fields[f]][a][fields[g]][b] +=
			  Qwts[q]*PenaltyParam*
			  (2.*tempval1*tempval2 + (dist2-cdist2)*tempval3);
		    }
	      }
      }
    return;
  }
    
  // Consistency test for this operation at the given configuration
  bool Ops3::ConsistencyTest(const void* arg,
			     const double pertEPS, const double tolEPS) const
  {
    const int SPD = 3;
    
    // Consistency test is for an element/operation with 6 fields only.
    assert(static_cast<int>(Elm->GetFields().size())==6);
    
    // Cast configuration pointer
    const BiRodConfiguration* ptr = static_cast<const BiRodConfiguration*>(arg);
    assert(ptr!=nullptr && "simorods::Ops3::ConsistencyTest- invalid configuration pointer");
    const BiRodConfiguration& Config = *ptr;
    assert(Config.IsInitialized() && "simorods::Ops3::ConsistencyTest- configuration not initialized");

    // Access the rods
    const auto& Rod1 = Config.GetRod1();
    const auto& Rod2 = Config.GetRod2();
    const double c2 = Config.GetConstraintDistance2();
    
    // Compute the residue and the derivative
    const int ndofs = Elm->GetDof(fields[0]);
    const int nfields = static_cast<int>(fields.size());
    std::vector<std::vector<double>> resvals(nfields, std::vector<double>(ndofs));
    std::vector<std::vector<std::vector<std::vector<double>>>>dresvals(nfields);
    for(int f=0; f<nfields; ++f)
      {
	dresvals[f].resize(ndofs);
	for(int a=0; a<ndofs; ++a)
	  {
	    dresvals[f][a].resize(nfields);
	    for(int g=0; g<nfields; ++g)
	      dresvals[f][a][g].resize(ndofs);
	  }
      }
    GetDVal(&Config, &resvals, &dresvals);

    // Compute the residue and dresidue numerically
    auto nresvals = resvals;
    auto ndresvals = dresvals;
    SetZero(&nresvals, &ndresvals);
    
    // Local residual calculations at perturbed configs
    auto pres = resvals;
    auto mres = resvals;
    
    // Compute the residual/dresidual numerically
    // Fields for centerline of rod1
    for(int f=0; f<SPD; ++f)
      for(int a=0; a<ndofs; ++a)
	{
	  // Create perturbed configurations here
	  Configuration pRod1(Rod1), mRod1(Rod1);
	  BiRodConfiguration pConfig(pRod1, Rod2, c2);
	  BiRodConfiguration mConfig(mRod1, Rod2, c2);

	  // Centerline update of rod1
	  Vec3 incPhi;
	  for(int i=0; i<3; ++i)
	    incPhi[i] = 0.;
	  incPhi[f] = pertEPS;
	  pRod1.UpdateCenterline(ConfigStr.nodes[a], incPhi);
	  pRod1.SetInitialized();

	  incPhi[f] = -pertEPS;
	  mRod1.UpdateCenterline(ConfigStr.nodes[a], incPhi);
	  mRod1.SetInitialized();

	  // No centerline update for rod2,
	  
	  // Compute perturbed energies
	  auto Eplus = GetEnergy(&pConfig);
	  auto Eminus = GetEnergy(&mConfig);
	  nresvals[f][a] = (Eplus-Eminus)/(2.*pertEPS);
	  
	  // Compute the perturbed residual
	  GetVal(&mConfig, &mres);
	  GetVal(&pConfig, &pres);

	  // Update the numerical stiffnesses
	  for(int g=0; g<nfields; ++g)
	    for(int b=0; b<ndofs; ++b)
	      ndresvals[g][b][f][a] = (pres[g][b]-mres[g][b])/(2.*pertEPS);
	}

    // Fields for centerline of rod2
    for(int f=0; f<SPD; ++f)
      for(int a=0; a<ndofs; ++a)
	{
	  // Create perturbed configurations here
	  Configuration pRod2(Rod2), mRod2(Rod2);
	  BiRodConfiguration pConfig(Rod1, pRod2, c2);
	  BiRodConfiguration mConfig(Rod1, mRod2, c2);

	  // Centerline update of rod2
	  Vec3 incPhi;
	  for(int i=0; i<3; ++i)
	    incPhi[i] = 0.;
	  incPhi[f] = pertEPS;
	  pRod2.UpdateCenterline(ConfigStr.nodes[a], incPhi);
	  pRod2.SetInitialized();

	  incPhi[f] = -pertEPS;
	  mRod2.UpdateCenterline(ConfigStr.nodes[a], incPhi);
	  mRod2.SetInitialized();

	  // No centerline update for rod1,
	  
	  // Compute perturbed energies
	  auto Eplus = GetEnergy(&pConfig);
	  auto Eminus = GetEnergy(&mConfig);
	  nresvals[f+SPD][a] = (Eplus-Eminus)/(2.*pertEPS);
	  
	  // Compute the perturbed residual
	  GetVal(&mConfig, &mres);
	  GetVal(&pConfig, &pres);

	  // Update the numerical stiffnesses
	  for(int g=0; g<nfields; ++g)
	    for(int b=0; b<ndofs; ++b)
	      ndresvals[g][b][f+SPD][a] = (pres[g][b]-mres[g][b])/(2.*pertEPS);
	}

    // Check consistency
    for(int f=0; f<nfields; ++f)
      for(int a=0; a<ndofs; ++a)
	{
	  assert(std::abs(resvals[f][a]-nresvals[f][a])<tolEPS &&
		 "simorods::Ops3::Consistency test for residuals failed");
	  for(int g=0; g<nfields; ++g)
	    for(int b=0; b<ndofs; ++b)
	      assert(std::abs(dresvals[f][a][g][b]-ndresvals[f][a][g][b])<tolEPS &&
		     "simorods::Ops3::Consistency test for dresiduals failed");
	}
    
    // For debugging puposes only
    if(0)
      {
	std::cout<<"\n\nConsistency of residuals: ";
	for(int f=0; f<nfields; ++f)
	  for(int a=0; a<ndofs; ++a)
	    std::cout<<"\n"<<resvals[f][a]<<" should be "<<nresvals[f][a];
	std::fflush( stdout );
	
	std::cout<<"\n\nConsistency of dresiduals: ";
	for(int f=0; f<nfields; ++f)
	  for(int a=0; a<ndofs; ++a)
	    for(int g=0; g<nfields; ++g)
	      for(int b=0; b<ndofs; ++b)
		std::cout<<"\n"<<dresvals[f][a][g][b]<<" should be "<<ndresvals[f][a][g][b];
	std::fflush( stdout );
      }
    return true;  
  }
  
}
