#ifndef NEW_POINT_KINEMATICS_H
#define NEW_POINT_KINEMATICS_H

#include <SimoRodsPointKinematics.h>
#include <cassert>

namespace simorods
{
  class NewPointKinematics
  {
  public:
    double phi1[3], phi2[3];

    // default constructor
    inline NewPointKinematics() {}

    // copy constructor
    inline NewPointKinematics(const NewPointKinematics& Obj)
    {
      for(int k=0; k<3; ++k)
	{ phi1[k] = Obj.phi1[k];
	  phi2[k] = Obj.phi2[k]; }
    }

    // destructor, does nothing
    inline virtual ~NewPointKinematics() {}

    // assignment operator
    inline NewPointKinematics& operator=(const NewPointKinematics& rhs)
      {
	if(&rhs==this) return *this;
	for(int k=0; k<3; ++k)
	  { phi1[k] = rhs.phi1[k];
	    phi2[k] = rhs.phi2[k]; }
	return *this;
      }
  };


  
  // for 1st variation of kinematics data at a point
  class NewVarPointKinematics
  {
  public:
    const NewPointKinematics* PD;
    double eta1[3];
    double eta2[3];

    // default constructor
    inline NewVarPointKinematics()
      :PD(nullptr)
    { SetZero(); }
    
    // copy constructor
    inline NewVarPointKinematics(const NewVarPointKinematics& Obj)
      :PD(Obj.PD)
    {
      for(int k=0; k<3; ++k)
	{ eta1[k] = Obj.eta1[k];
	  eta2[k] = Obj.eta2[k]; }
    }
    
    // destructor, does nothing
    inline virtual ~NewVarPointKinematics() {}
    
    // assignment operator
    inline NewVarPointKinematics& operator=(const NewVarPointKinematics& rhs) = delete;
    //{ assert(false && "Should not need to assign variation of point data"); }

    // Set all variations to zero
    void SetZero()
    {
      for(int k=0; k<3; ++k)
	{ eta1[k] = 0.;
	  eta2[k] = 0.; }
    }
  };

}


#endif
