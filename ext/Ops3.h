
// Sriramajayam

#ifndef OPS3_H
#define OPS3_H

#include <BiRodConfiguration.h>
#include <Ops3Workspace.h>
#include <SimoRodsOpsWorkspace.h>
#include <ElementalOperation.h>

namespace simorods
{
  // class for computing energy, stiffness,
  // force residual for a system consisting two simo-rods and constraints betwen them
  class Ops3: public DResidue
  {
  public:
    // constructor
    inline Ops3(const Element* elm,
		const std::vector<int> fieldnums,
		ConfigAccessStruct str, 
		Ops3Workspace* wrkspc,
		const double kval)
      :DResidue(),
      Elm(elm),
      fields(fieldnums),
      ConfigStr(str),
      WrkSpc(wrkspc),
      PenaltyParam(kval)
      { assert (static_cast<int>(fields.size())==6 &&
		"simorods::Ops3::Ops3 - expected 6 fields"); }

    // destructor, does nothing
    inline virtual ~Ops3() {}

    // Copy constructor
    inline Ops3(const Ops3& Obj)
      :DResidue(Obj),
      Elm(Obj.Elm),
      fields(Obj.fields),
      ConfigStr(Obj.ConfigStr),
      WrkSpc(Obj.WrkSpc),
      PenaltyParam(Obj.PenaltyParam) {}

    // Cloning
    inline virtual Ops3* Clone() const override
    { return new Ops3(*this); }
    
    // Disable assignment
    Ops3& operator=(const Ops3&) = delete;

    // Returns the element being used
    inline const Element* GetElement() const
    { return Elm; }

    // Return the fields used
    inline const std::vector<int>& GetField() const override
    { return fields; }

    // Returns the number of dofs for a given field
    inline int GetFieldDof(int fieldnumber) const override
    { return Elm->GetDof(fields[fieldnumber]); }

    // Retrun the value of the penalty parameter
    inline const double GetPenaltyParameter() const
    { return PenaltyParam; }

    // Computes the energy functional
    double GetEnergy(const void* Config) const;
    
    // Residual calculation
    void GetVal(const void* Config,
		std::vector<std::vector<double>>* resval) const;
    
    // Residual and stiffness calculation
    void GetDVal(const void* Config, 
		 std::vector<std::vector<double>>* resval,
		 std::vector<std::vector<std::vector<std::vector<double>>>>* dresval) const;
    
    // Consistency test for this operation at the given configuration
    // Assumes that the operation is for 7 fields only.
    bool ConsistencyTest(const void* Config,
			 const double pertEPS, const double tolEPS) const;
    

    
  private:
    const Element* Elm; // Element from the parametric configuration of both rods
    std::vector<int> fields; // fields for the operation
    const ConfigAccessStruct ConfigStr; // details to access configuration for this operation
    Ops3Workspace* WrkSpc; // Workspace to be used for calculations
    const double PenaltyParam; // distance b/w centerlines of two rods
  };
  
}

#endif
