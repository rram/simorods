// Sriramajayam

#include <BiRod.h>
#include <iostream>

using namespace simorods;

// Create a straight rod along Ez
void Initialize(const std::vector<double>& coordinates,
		const double yoffset,
		Configuration& Rod);

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a 1D mesh in the parametric configuration
  const int nNodes = 50;
  const int nElements = nNodes-1;
  std::vector<double> coord(nNodes);
  std::vector<int> conn(nElements*2);
  for(int n=0; n<nNodes; ++n)
    coord[n] = static_cast<double>(n)/static_cast<double>(nNodes-1);
  for(int e=0; e<nElements; ++e)
    { conn[2*e] = e+1;
      conn[2*e+1] = e+2; }

  // Material constants
  MatConstants MC(1., 1., 0.01, 0.01); // EA, GA, EI, GJ

  // Penalty parameter
  const double PenaltyParam = 1.e3;

  // Rod offset
  const double offset = 0.1;
  
  // Create the simulation manager
  BiRod birod(coord, conn, offset, MC, PenaltyParam);

  // Set the initial railtrack configuration
  birod.InitializeRailTrackConfiguration();

  // Plot the reference configuration
  birod.Plot("ref.dat");   // Both rods with connections

  // Clamp the left end
  std::vector<int> boundary(12);
  for(int i=0; i<12; ++i)
    boundary[i] = i;
  std::vector<double> bvalues(12);
  for(int i=0; i<12; ++i)
    bvalues[i] = 0.;

  // Tip the right end of both beams along the x-direction
  const Vec3 dx({0.05,0.,0.});
  boundary.push_back( 12*(nNodes-1)+0 );
  boundary.push_back( 12*(nNodes-1)+6 );
  bvalues.push_back(0.);
  bvalues.push_back(0.);

  // Load stepping
  for(int step=0; step<10; ++step)
    {
      std::cout<<"\nLoad step: "<<step<<std::flush;
      // Displace the right end
      auto& Rod1 = birod.GetRod1(); Rod1.UpdateCenterline(nNodes-1, dx); Rod1.SetInitialized();
      auto& Rod2 = birod.GetRod2(); Rod2.UpdateCenterline(nNodes-1, dx); Rod2.SetInitialized();

      // Solve
      birod.Solve(boundary, bvalues, 50); // max: 50 iterations

      // Plot
      birod.Plot("d"+std::to_string(step+1)+".dat");
    }

  // -- done --
}

