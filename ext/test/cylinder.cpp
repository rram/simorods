// Sriramajayam

#include <BiRod.h>
#include <iostream>

using namespace simorods;

// Create a straight rod along Ez
void Initialize(const std::vector<double>& coordinates,
		const double yoffset,
		Configuration& Rod);

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a 1D mesh in the parametric configuration
  const int nNodes = 100;
  const int nElements = nNodes-1;
  std::vector<double> coord(nNodes);
  std::vector<int> conn(nElements*2);
  for(int n=0; n<nNodes; ++n)
    coord[n] = static_cast<double>(n)/static_cast<double>(nNodes-1);
  for(int e=0; e<nElements; ++e)
    { conn[2*e] = e+1;
      conn[2*e+1] = e+2; }

  // Material constants
  MatConstants MC(1., 1., 0.001, 0.001); // EA, GA, EI, GJ

  // Penalty parameter
  const double PenaltyParam = 1.e3;

  // Rod offset
  const double offset = 0.2;
  
  // Create the simulation manager
  BiRod birod(coord, conn, offset, MC, PenaltyParam);
  //auto& Rod1 = birod.GetRod1();
  //auto& Rod2 = birod.GetRod2();

  // Set the initial railtrack configuration
  birod.InitializeRailTrackConfiguration();

  // Plot the reference configuration
  birod.Plot("ref.dat");   // Both rods with connections
  birod.PlotTec("ref.tec");   // Both rods with connections
  
  // Clamp the left end
  std::vector<int> left_boundary(12);
  for(int i=0; i<12; ++i)
    left_boundary[i] = i;
  std::vector<double> left_bvalues(12);
  for(int i=0; i<12; ++i)
    left_bvalues[i] = 0.;
  
  // Load stepping for rotation about the Y axis
  const double PI = 4.*std::atan(1.);
  for(int step=0; step<20; ++step)
    {
      std::cout<<"\nY rotation load step: "<<step<<std::flush;

      // Right side bcs
      std::vector<int> boundary = left_boundary;
      boundary.push_back(12*(nNodes-1)+3+1);
      boundary.push_back(12*(nNodes-1)+9+1);
      std::vector<double> bvalues = left_bvalues;
      bvalues.push_back(2.*PI/20.);
      bvalues.push_back(2.*PI/20.);

      // 1 iteration that sets dt
      birod.Solve(boundary, bvalues, 1);

      // Remaining iterations to equilibrium
      std::fill(bvalues.begin(), bvalues.end(), 0.);
      birod.Solve(boundary, bvalues, 50);
    }

  // Plot the realized solution
  birod.Plot("cyl.dat");
  birod.PlotTec("cyl.tec");   

  // Freeze y rotations in the current state. Sanity check.
  {
    std::vector<int> boundary = left_boundary;
    boundary.push_back(12*(nNodes-1)+3+1);
    boundary.push_back(12*(nNodes-1)+9+1);
    std::vector<double> bvalues = left_bvalues;
    bvalues.push_back(0.);
    bvalues.push_back(0.);
    birod.Solve(boundary, bvalues, 50);
  }

  // Freeze both X & Y rotations
  {
     std::vector<int> boundary = left_boundary;
    boundary.push_back(12*(nNodes-1)+3);
    boundary.push_back(12*(nNodes-1)+3+1);
    boundary.push_back(12*(nNodes-1)+9);
    boundary.push_back(12*(nNodes-1)+9+1);
    std::vector<double> bvalues = left_bvalues;
    bvalues.push_back(0.);
    bvalues.push_back(0.);
    bvalues.push_back(0.);
    bvalues.push_back(0.);
    birod.Solve(boundary, bvalues, 50);
  }

  // Freeze X rotation
  {
    std::vector<int> boundary = left_boundary;
    boundary.push_back(12*(nNodes-1)+3);
    boundary.push_back(12*(nNodes-1)+9);
    std::vector<double> bvalues = left_bvalues;
    bvalues.push_back(0.);
    bvalues.push_back(0.);
    birod.Solve(boundary, bvalues, 1);
  }
  birod.Plot("recyl.dat");
  birod.PlotTec("recyl.tec");   
   
  // -- done --
}
