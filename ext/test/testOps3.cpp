// Sriramajayam

#include <cassert>
#include <iostream>
#include <SimoRodsP11DElement.h>
#include <Ops3.h>
#include <random>

using namespace simorods;

// Random double 
std::random_device rd;  //Will be used to obtain a seed for the random number engine
std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
std::uniform_real_distribution<> dis(0.,1.);

// Generate a random vector
Vec3 GenerateVector();

int main()
{
  // Create a 1-element configuration
  const int nNodes = 2;
  const int nElms = 1;
  const int nQuadsPerElm = 2;
  const double c2 = std::abs(dis(gen));
  const double PenaltyParam = std::abs(dis(gen));
  Configuration Rod1(nNodes, nElms, nQuadsPerElm);
  Configuration Rod2(nNodes, nElms, nQuadsPerElm);
  BiRodConfiguration Config(Rod1, Rod2, c2);
  
  // Set phi1, phi2 and SmallLambda i.e. centerline coordinates of both the rods and Lagrange multiplier for the rods
  for (int a=0; a<nNodes; ++a)
    {
      auto vec1 = GenerateVector();
      auto vec2 = GenerateVector();
      Rod1.SetCenterline(a, vec1); // Rotations and Omega are not required
      Rod2.SetCenterline(a, vec2);
    }
  Rod1.SetInitialized();
  Rod2.SetInitialized();
  
  // Create 1 element on the reference line
  std::vector<double> Coordinates({dis(gen), 1.2+dis(gen)});
  Segment<1>::SetGlobalCoordinatesArray(Coordinates);
  const int nFields = 6;
  P11DElement<nFields> Elm(1,2);
  
  // Workspace for operations
  const int ndofs = 2;
  ConfigAccessStruct Str(std::vector<int>{0,1}, 0, nQuadsPerElm);
  Ops3Workspace WrkSpc(nFields*ndofs);
  
  // Create an operation
  Ops3 Op3(&Elm, std::vector<int>({0,1,2,3,4,5}), Str, &WrkSpc, PenaltyParam);
  
  // Compute the energy, residual and dresidual at the given configuration
  std::vector<std::vector<double>> resval(nFields, std::vector<double>(ndofs));
  std::vector<std::vector<std::vector<std::vector<double>>>> dresval(nFields);
  for(int f=0; f<nFields; ++f)
    {
      dresval[f].resize(ndofs);
      for(int a=0; a<ndofs; ++a)
	{
	  dresval[f][a].resize(nFields);
	  for(int g=0; g<nFields; ++g)
	    dresval[f][a][g].resize(ndofs);
	}
    }
  Op3.GetEnergy(&Config); // return value ignored
  Op3.GetVal(&Config, &resval);
  Op3.GetDVal(&Config, &resval, &dresval);
  
  // Consistency test
  assert(Op3.ConsistencyTest(&Config, 1.e-4, 1.e-6));

  // cout the result of consistency test
  std::cout<< "It has passed the consistency test ";
}


// Generate a random vector
Vec3 GenerateVector()
{
  Vec3 vec;
  for(int i=0; i<3; ++i)
    vec[i] = dis(gen);

  return vec;
}  
