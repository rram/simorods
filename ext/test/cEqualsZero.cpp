// Sriramajayam

// Implementation with c = 0
#include <SimoRods_Module>
#include <BiRodConfiguration.h>
#include <CombinedOps.h>
#include <PetscData.h>

using namespace simorods;

// Plot a given configuration
void PlotConfiguration(const std::string filename,
		       const Configuration& Rod);

// Create a straight rod along Ez
void InitializeRod(const std::vector<double>& coordinates,
		   const double yoffset, Configuration& Rod);

// Update rod configuration
void UpdateRod(const int rodnum,
	       Configuration& Rod,
	       const std::vector<Element*>& ElmArray,
	       const double* sol);

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a 1D reference mesh over [0,1]
  const int nRefNodes = 50;
  const int nRefElements = nRefNodes-1;
  std::vector<double> RefCoordinates(nRefNodes);
  std::vector<int> RefConnectivity(nRefElements*2);
  for(int n=0; n<nRefNodes; ++n)
    RefCoordinates[n] = static_cast<double>(n)/static_cast<double>(nRefNodes-1);
  for(int e=0; e<nRefElements; ++e)
    { RefConnectivity[2*e] = e+1;
      RefConnectivity[2*e+1] = e+2; }
  
  // Set the global coordinates array
  Segment<1>::SetGlobalCoordinatesArray(RefCoordinates);

  // Create elements over the reference line (different from the initial )
  std::vector<Element*> ElmArray(nRefElements);
  const int nFields = 12; // 3 centerline + 3 directors + 3 centerline + 3 directors
  for(int e=0; e<nRefElements; ++e)
    ElmArray[e] = new P11DElement<nFields>(RefConnectivity[2*e], RefConnectivity[2*e+1]);

  // Local to Global Map
  StandardP11DMap L2GMap(ElmArray);

  // Create configurations for 2 rods
  const double yoffset = 0.25;
  const int nQuadsPerRefElm = static_cast<int>(ElmArray[0]->GetIntegrationWeights(0).size());
  Configuration Rod1(nRefNodes, nRefElements, nQuadsPerRefElm);
  Configuration Rod2(nRefNodes, nRefElements, nQuadsPerRefElm);
  InitializeRod(RefCoordinates, 0., Rod1);
  InitializeRod(RefCoordinates, yoffset, Rod2);
  
  // Birod
  const double cDist2 = yoffset*yoffset;
  BiRodConfiguration Config(Rod1, Rod2, cDist2);
  PlotConfiguration("refR1.dat", Rod1);
  PlotConfiguration("refR2.dat", Rod2);
    
  // Create material
  MatConstants MC(1., 1., 0.01, 0.01); // EA, GA, EI, GJ
  Material SMat(MC);

  // Penalty parameter
  const double PenaltyParam = 100.;
  
  // Workspaces for operation-level calculations
  OpsWorkspace WrkSpc1(6);// 6 is the max no. of 1st variations for Ops of rod1
  OpsWorkspace WrkSpc2(6);// 6 is the max no. of 1st variations for Ops of rod2
  Ops3Workspace WrkSpc3(6);// 6 is the max no. of 1st variations for the Ops3
  
  // Create Operations
  std::vector<CombinedOps*> CombinedOpArray(nRefElements);
  std::vector<int> FieldNums({0,1,2,3,4,5,6,7,8,9,10,11}); 
    for(int e=0; e<nRefElements; ++e)
    {
      // How to access the configuration 
      ConfigAccessStruct str(std::vector<int>({RefConnectivity[2*e]-1, RefConnectivity[2*e+1]-1}), e, nQuadsPerRefElm);
      // Create operation
      CombinedOpArray[e] = new CombinedOps(ElmArray[e], FieldNums, str, SMat, &WrkSpc1, &WrkSpc2, &WrkSpc3, PenaltyParam);
    }
  
  // Create assembler
  StandardAssembler<CombinedOps> Asm(CombinedOpArray, L2GMap);
  
  // Create PETSc data structures
  PetscData PD;
  
  // Estimate number of nonzeros in each row
  const int nTotalDof = L2GMap.GetTotalNumDof();
  std::vector<int> nnz(nTotalDof);
  Asm.CountNonzeros(nnz);
  PD.Initialize(nnz);
  
  // Set a direct solver
  PC pc;
  KSPGetPC(PD.kspSOLVER, &pc);
  PCSetType(pc, PCLU);
  
  // Dirichlet boundary conditions
  std::vector<int> boundary;
  // Fix the left end
  for(int i=0; i<12; ++i)
    { boundary.push_back(0*nFields+i); }
  // Displace the right end along x direction
  boundary.push_back((nRefNodes-1)*nFields+0);
  boundary.push_back((nRefNodes-1)*nFields+6);
  const Vec3 dx1({0.05, 0.,0.});
  const Vec3 dx2({-0.05, 0.,0.});

  std::vector<double> bvalues(boundary.size());
  std::fill(bvalues.begin(), bvalues.end(), 0.);

  // Load stepping
  for(int step=0; step<15; ++step)
    {
      // Displace the right end
       Rod1.UpdateCenterline(nRefNodes-1, dx1); Rod1.SetInitialized();
       Rod2.UpdateCenterline(nRefNodes-1, dx2); Rod2.SetInitialized();

      // Newton-Raphson
      int iter = 0;
      while(true)
	{
	  std::cout<<"\nIteration "<<++iter<<": "<<std::flush;
	  
	  // Assemble the matrix-vector system at the current configuration
	  Asm.Assemble(&Config, PD.resVEC, PD.stiffnessMAT);

	  // Set Dirichlet BCs
	  PD.SetDirichletBCs(boundary, bvalues);

	  // Solve
	  PD.Solve();
	  
	  // Check convergence
	  if(PD.HasConverged(1.e-10, 1., 1.))
	    { std::cout<<"\nConverged!\n"<<std::flush; break; }
	  
	  // Correct the sign of increments
	  VecScale(PD.solutionVEC, -1.);

	  // Get the increments
	  double* sol;
	  VecGetArray(PD.solutionVEC, &sol);
	  UpdateRod(1, Rod1, ElmArray, sol);
	  UpdateRod(2, Rod2, ElmArray, sol);
	  VecRestoreArray(PD.solutionVEC, &sol);

	  // Go to the next iteration
	} // End Newton-Raphson
      
      // Plot the deformed configuration
      std::string filename;
      filename = "defR1-"+std::to_string(step) + ".dat";
      PlotConfiguration(filename, Rod1);
      filename = "defR2-"+std::to_string(step) + ".dat";
      PlotConfiguration(filename, Rod2);
      
    } // End Load stepping
  
  // Clean up
  for(auto& x:ElmArray) delete x;
  for(auto& x:CombinedOpArray) delete x;
  PD.Destroy();
  
  // Finalize PETSc
  PetscFinalize();
}


// Create a straight rod along Ez
void InitializeRod(const std::vector<double>& coordinates,
		   const double yoffset, Configuration& Rod)
{
  int nNodes = Rod.GetNumNodes();
  int nElements = Rod.GetNumElements();
  int nQuad = Rod.GetNumQuadraturePointsPerElement();
  
  Vec3 phival({0.,yoffset,0.}); 
  for(int n=0; n<nNodes; ++n)
    { phival[2] = coordinates[n];
      Rod.SetCenterline(n, phival); }

  // Set rotations & vorticity
  Mat3 Rot, Omega;
  for(int i=0; i<3; ++i)
    {
      for(int j=0; j<3; ++j)
	{ Rot[i][j] = 0.;
	  Omega[i][j] = 0.; }
      Rot[i][i] = 1.;
    }
  
  for(int e=0; e<nElements; ++e)
    for(int q=0; q<nQuad; ++q)
      {
	Rod.SetRotation(e, q, Rot);
	Rod.SetVorticity(e, q, Omega);
      }
  Rod.SetInitialized();
  return;
}



// Plot a given configuration
void PlotConfiguration(const std::string filename,
		       const Configuration& Config)
{
  std::fstream pfile;
  pfile.open(filename.c_str(), std::ios::out);
  assert(pfile.good());
  const int nNodes = Config.GetNumNodes();
  const auto& phi = Config.GetCenterline();
  for(int n=0; n<nNodes; ++n)
    pfile<<phi[n][0]<<"\t"<<phi[n][1]<<"\t"<<phi[n][2]<<"\n";
  pfile.flush();
  pfile.close();
}


// Update rod configuration
void UpdateRod(const int rodnum,
		Configuration& Rod,
		const std::vector<Element*>& ElmArray,
		const double* sol)
{
  assert(rodnum==1 || rodnum==2);
  const int foffset = (rodnum==1)? 0 : 6; // Fields start from 0 for rod 1, 6 for rod 2
    
  const int nNodes = Rod.GetNumNodes();
  const int nElements = Rod.GetNumElements();
  const int nQuad = Rod.GetNumQuadraturePointsPerElement();

  Vec3 dphi;
  double nodal_theta[2][3];
  Vec3 theta, dtheta;
  
  // Update centerlines
  for(int n=0; n<nNodes; ++n)
    {
      for(int i=0; i<3; ++i)
	dphi[i] = sol[12*n+foffset+i];
      Rod.UpdateCenterline(n, dphi);
    }
	  
  // Update rotations and vorticities
  for(int e=0; e<nElements; ++e)
    {
      const auto& Conn = ElmArray[e]->GetElementGeometry().GetConnectivity();
      const int a = Conn[0]-1;
      const int b = Conn[1]-1;

      // Rod theta @ left + right nodes
      for(int f=0; f<3; ++f)
	{
	  nodal_theta[0][f] = sol[12*a+foffset+3+f];
	  nodal_theta[1][f] = sol[12*b+foffset+3+f];
	}
      
      // Quadrature-point-wise-update
      for(int q=0; q<nQuad; ++q)
	{
	  theta[0] = theta[1] = theta[2] = 0.;
	  dtheta[0] = dtheta[1] = dtheta[2] = 0.;
	  for(int a=0; a<2; ++a)
	    for(int k=0; k<3; ++k)
	      {
		theta[k] += nodal_theta[a][k]*ElmArray[e]->GetShape(0,q,a);
		dtheta[k] += nodal_theta[a][k]*ElmArray[e]->GetDShape(0,q,a,0);
	      }
	  Rod.UpdateRotations(e, q, theta, dtheta);
	}
    }
  Rod.SetInitialized();

  return;
}
