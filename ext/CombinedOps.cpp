// Sriramajayam

#include <CombinedOps.h>
#include <SimoRodsPointKinematics.h>
#include <NewPointKinematics.h>
#include <SimoRodsUtils.h>
#include <cmath>
#include <cassert>
#include <iostream>

namespace simorods
{
  // Constructor
  CombinedOps::CombinedOps(const Element* elm,
			   const std::vector<int> fieldnums,
			   ConfigAccessStruct str,
			   const Material& mat,
			   OpsWorkspace* wrkspc1,
			   OpsWorkspace* wrkspc2,
			   Ops3Workspace* wrkspc3,
			   const double kVal)
    :DResidue(),
     fields(fieldnums),
     Op1(elm,std::vector<int>({fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]}),str,mat,wrkspc1),
     Op2(elm,std::vector<int>({fields[6],fields[7],fields[8],fields[9],fields[10],fields[11]}),str,mat,wrkspc2),
     Op3(elm,std::vector<int>({fields[0],fields[1],fields[2],fields[6],fields[7],fields[8]}),str,wrkspc3,kVal)
  {
    // Assert on fields numbering her
    const int nFields = static_cast<int>(fieldnums.size());
    assert (nFields==12);
    for(int f=0; f<nFields; ++f)
      assert(fieldnums[f]==f);
    
    // Resize resval_1, resval_2, dresval_1 dresval_2
    resval_1.resize(nFields);
    resval_2.resize(nFields);
    dresval_1.resize(nFields);
    dresval_2.resize(nFields);
    for (int f=0; f<nFields; ++f)
      {
	int nDof_f = elm->GetDof(f);
	resval_1[f].resize(nDof_f);
	resval_2[f].resize(nDof_f);
	dresval_1[f].resize(nDof_f);
	dresval_2[f].resize(nDof_f);
	for(int a=0; a<nDof_f; ++a)
	  {
	    dresval_1[f][a].resize(nFields);
	    dresval_2[f][a].resize(nFields);
	    for(int g=0; g<nFields; ++g)
	      {
		dresval_1[f][a][g].resize(elm->GetDof(g));
		dresval_2[f][a][g].resize(elm->GetDof(g));
	      }
	  }
      }
  }
  
    
  // Copy Constructor
  CombinedOps::CombinedOps(const CombinedOps& Obj)
    :DResidue(Obj),
     fields(Obj.fields),
     Op1(Obj.Op1),
     Op2(Obj.Op2),
     Op3(Obj.Op3),
     resval_1(Obj.resval_1),
     resval_2(Obj.resval_2),
     dresval_1(Obj.dresval_1),
     dresval_2(Obj.dresval_2) {}

  
  // Net Energy calculation
  double CombinedOps::GetEnergy(const void* arg) const
  {
    // Cast Configuration
    assert(arg!=nullptr);
    const BiRodConfiguration* Config = static_cast<const BiRodConfiguration*>(arg);
    assert(Config!=nullptr && "simorods::CombinedOps::GetEnergy- Invalid pointer to configration13");
    
    // Check that this configuration is "set"
    assert(Config->IsInitialized() && "CombinedOps::GetEnergy()- Configuration not initialized");
    
    // Get the rods
    const auto& Rod1 = Config->GetRod1();
    const auto& Rod2 = Config->GetRod2();
    
    // NetEnergy to be computed
    double E1 = Op1.GetEnergy(&Rod1);
    double E2 = Op2.GetEnergy(&Rod2);
    double E3 = Op3.GetEnergy(arg); 
    
    return E1+E2+E3;
  }
  
  
  // Combined residual calculation
  void CombinedOps::GetVal(const void* arg, 
			   std::vector<std::vector<double>>* resval) const
  { GetDVal(arg, resval, nullptr); }
  
  
  // Combined residual and stiffness calculation
  void CombinedOps::GetDVal(const void* arg, 
			    std::vector<std::vector<double>>* resval,
			    std::vector<std::vector<std::vector<std::vector<double>>>>* dresval) const
  {
    // Cast Configuration variable
    assert(arg!=nullptr);
    const BiRodConfiguration* Config = static_cast<const BiRodConfiguration*>(arg);
    assert(Config!=nullptr && "simorods::CombinedOps::GetDVal- Invalid configuration pointer");
    
    // Check that this provided configuration has been set
    assert(Config->IsInitialized() &&
	   "simorods::CombinedOps::GetDResidue()- Configuration not initialized.");
    
    // Initialize residual and stiffness to zero
    SetZero(resval, dresval);
    SetZero(&resval_1, &dresval_1);
    SetZero(&resval_2, &dresval_2);

    // Fields and dofs
    const int nfields = static_cast<int>(fields.size());
    std::vector<int> ndofs(nfields);
    for(int f=0; f<nfields; ++f)
      ndofs[f] = GetFieldDof(f);
    
    // Assign the pointers to the residuals of Op1, Op2, op3
    // Creating temporary variables
    const auto& Rod1 = Config->GetRod1();
    const auto& Rod2 = Config->GetRod2();

    Op1.GetDVal(&Rod1, &resval_1, &dresval_1); 
    Op2.GetDVal(&Rod2, &resval_2, &dresval_2);
    Op3.GetDVal(arg, resval, dresval);

    if(resval!=nullptr)
      for(int f=0; f<nfields; ++f)
	for(int a=0; a<ndofs[f]; ++a)
	  (*resval)[f][a] += resval_1[f][a] + resval_2[f][a];
    
    if(dresval!=nullptr)
      for(int f=0; f<nfields; ++f)
	for(int a=0; a<ndofs[f]; ++a)
	  for(int g=0; g<nfields; ++g)
	    for(int b=0; b<ndofs[g]; ++b)
	      (*dresval)[f][a][g][b] += dresval_1[f][a][g][b] + dresval_2[f][a][g][b];

    return;
  }
}


   
