// Sriramajayam

#include <BiRod.h>
#include <cassert>
#include <iostream>
#include <fstream>
#include <PlottingUtils.h>

namespace simorods
{
  //! Constructor
  BiRod::BiRod(const std::vector<double>& coord,
	       const std::vector<int>& conn,
	       const double yoff,
	       const MatConstants MC,
	       const double penalty)
    :coordinates(coord),
     connectivity(conn),
     yoffset(yoff),
     Mtl(MC),
     PenaltyParam(penalty)
  {
    // Check that PETSc is initialized
    PetscBool flag;
    PetscInitialized(&flag);
    assert(flag==PETSC_TRUE);
    
    // Number of nodes and elements
    const int nNodes = static_cast<int>(coord.size());
    const int nElements = static_cast<int>(conn.size()/2);
    assert(nNodes==nElements+1 && "Inconsistent number of nodes and elements");
    
    // Create elements
    Segment<1>::SetGlobalCoordinatesArray(coordinates);
    ElmArray.resize(nElements);
    const int nFields = 12; // 3 centerline + 3 directors + 3 centerline + 3 director
    for(int e=0; e<nElements; ++e)
      ElmArray[e] = new P11DElement<nFields>(connectivity[2*e], connectivity[2*e+1]);

    // Local to global map
    L2GMap = new StandardP11DMap(ElmArray);

    // Configurations for the two rods
    const int nQuadsPerElm = static_cast<int>(ElmArray[0]->GetIntegrationWeights(0).size());
    Rod1 = new Configuration(nNodes, nElements, nQuadsPerElm);
    Rod2 = new Configuration(nNodes, nElements, nQuadsPerElm);
    birod = new BiRodConfiguration(*Rod1, *Rod2, yoffset*yoffset);

    // Create operations
    WrkSpc1 = new OpsWorkspace(6);
    WrkSpc2 = new OpsWorkspace(6);
    WrkSpc3 = new Ops3Workspace(6);
    OpArray.resize(nElements);
    std::vector<int> FieldNums({0,1,2,3,4,5,6,7,8,9,10,11});
    for(int e=0; e<nElements; ++e)
      {
	// Accessing the configuraiton
	ConfigAccessStruct str(std::vector<int>({connectivity[2*e]-1, connectivity[2*e+1]-1}),
			       e, nQuadsPerElm);

	// Create operation
	OpArray[e] = new CombinedOps(ElmArray[e], FieldNums, str, Mtl,
				     WrkSpc1, WrkSpc2, WrkSpc3, PenaltyParam);
      }

    // Create assembler
    Asm = new StandardAssembler<CombinedOps>(OpArray, *L2GMap);

    // Create PETSc data structures
    const int nTotalDof = L2GMap->GetTotalNumDof();
    std::vector<int> nnz(nTotalDof);
    Asm->CountNonzeros(nnz);
    PD.Initialize(nnz);

    // Set a direct solver
    PC pc;
    KSPGetPC(PD.kspSOLVER, &pc);
    PCSetType(pc, PCLU);

    // -- done --
  }


  // Clean up
  BiRod::~BiRod()
  { Destroy(); }

  void BiRod::Destroy()
  {
    for(auto& x:ElmArray) delete x;
    for(auto& x:OpArray) delete x;
    delete L2GMap;
    delete Rod1;
    delete Rod2;
    delete birod;
    delete Asm;
    delete WrkSpc1;
    delete WrkSpc2;
    delete WrkSpc3;
    PD.Destroy();
  }

  // Access to coordinates
  const std::vector<double>& BiRod::GetCoordinates() const
  { return coordinates; }

  // Access to connectivity
  const std::vector<int>& BiRod::GetConnectivity() const
  { return connectivity; }

  // Access the number of elements
  int BiRod::GetNumElements() const
  { return static_cast<int>(connectivity.size()/2); }
  
  // Access the number of nodes
  int BiRod::GetNumNodes() const
  { return static_cast<int>(coordinates.size()); }

  // Access the configuration of rod 1
  Configuration& BiRod::GetRod1()
  { return *Rod1; }

  // Access the configuration of rod 2
  Configuration& BiRod::GetRod2()
  { return *Rod2; }

  // Access the element array
  const std::vector<Element*>& BiRod::GetElementArray() const
  { return ElmArray; }

  // Access the local to global map
  const LocalToGlobalMap& BiRod::GetLocalToGlobalMap() const
  { return *L2GMap; }

  // Access the assembler
  const StandardAssembler<CombinedOps>& BiRod::GetAssembler() const
  { return *Asm; }

  // Access the material
  const Material& BiRod::GetMaterial() const
  { return Mtl; }

  // Access the penalty parameter
  double BiRod::GetPenaltyParameter() const
  { return PenaltyParam; }

  // Main functionality
  void BiRod::Solve(const std::vector<int>& boundary,
		    const std::vector<double>& bvalues,
		    const int nMaxIters)
  {
    // Sanity checks
    assert(boundary.size()==bvalues.size());
    assert(Rod1->IsInitialized() && Rod2->IsInitialized());
    assert(nMaxIters>=1);

    // Newton-Raphson
    PetscErrorCode ierr;
    int iter = 0;
    std::cout<<"\n"<<std::flush;
    while(iter<nMaxIters)
      {
	++iter;
	std::cout<<"Iteration "<<iter<<": "<<std::flush;
	Asm->Assemble(birod, PD.resVEC, PD.stiffnessMAT);
	PD.SetDirichletBCs(boundary, bvalues);
	PD.Solve();
	if(PD.HasConverged(1.e-10, 1., 1.))
	  { std::cout<<"\nConverged!\n"<<std::flush; break; }

	// Switch the sign of increments
	VecScale(PD.solutionVEC, -1.);

	// Get the increments
	double* sol;
	ierr = VecGetArray(PD.solutionVEC, &sol); CHKERRV(ierr);
	UpdateRod(1, sol);
	UpdateRod(2, sol);
	ierr = VecRestoreArray(PD.solutionVEC, &sol); CHKERRV(ierr);

	// Go to the next iteration
      }

    //if(iter==nMaxIters)
    //{
	// This iteration did not work
    //	Destroy();
    //	assert(iter<nMaxIters && "Load step failed to converge");
    //}
    
    // -- done --
    return;
  }


  
  // Helper function to update the configuration of a rod
  void BiRod::UpdateRod(const int rodnum, const double* sol)
  {
    assert(rodnum==1 || rodnum==2);
    const int foffset = (rodnum==1) ? 0 : 6; // Fields start from 0 for rod 1, 6 for rod 2
    Configuration& Rod = (rodnum==1) ? *Rod1 : *Rod2;
    
    const int nNodes = GetNumNodes();
    const int nElements = GetNumElements();
    const int nQuad = Rod.GetNumQuadraturePointsPerElement();
    Vec3 dphi;
    double nodal_theta[2][3];
    Vec3 theta, dtheta;
    
    // Update centerlines
    for(int n=0; n<nNodes; ++n)
      {
	for(int i=0; i<3; ++i)
	  dphi[i] = sol[12*n+foffset+i];
	Rod.UpdateCenterline(n, dphi);
      }

    // Update rotations and vorticities
    for(int e=0; e<nElements; ++e)
      {
	const auto& Conn = ElmArray[e]->GetElementGeometry().GetConnectivity();
	const int a = Conn[0]-1;
	const int b = Conn[1]-1;
	
	// Rod theta @ left + right nodes
	for(int f=0; f<3; ++f)
	  {
	    nodal_theta[0][f] = sol[12*a+foffset+3+f];
	    nodal_theta[1][f] = sol[12*b+foffset+3+f];
	  }

	// Quadrature-point-wise-update
	for(int q=0; q<nQuad; ++q)
	  {
	    theta[0] = theta[1] = theta[2] = 0.;
	    dtheta[0] = dtheta[1] = dtheta[2] = 0.;
	    for(int a=0; a<2; ++a)
	      for(int k=0; k<3; ++k)
		{
		  theta[k] += nodal_theta[a][k]*ElmArray[e]->GetShape(0,q,a);
		  dtheta[k] += nodal_theta[a][k]*ElmArray[e]->GetDShape(0,q,a,0);
		}
	    Rod.UpdateRotations(e, q, theta, dtheta);
	  }
      }
    Rod.SetInitialized();
    
    // -- done --
    return;
  }

  
  // Plots the centerlines and nodal connections
  void BiRod::Plot(const std::string filename) const
  {
    std::fstream pfile;
    pfile.open(filename.c_str(), std::ios::out);
    const int nNodes = GetNumNodes();
    const auto& phi1 = Rod1->GetCenterline();
    const auto& phi2 = Rod2->GetCenterline();
    // Plot the two centerlines + intermediate ribs
    for(int n=0; n<nNodes; ++n)
      pfile<<phi1[n][0]<<" "<<phi1[n][1]<<" "<<phi1[n][2]<<"\n";
    for(int n=nNodes-1; n>=0; --n)
      pfile<<phi2[n][0]<<" "<<phi2[n][1]<<" "<<phi2[n][2]<<"\n"
	   <<phi1[n][0]<<" "<<phi1[n][1]<<" "<<phi1[n][2]<<"\n"
	   <<phi2[n][0]<<" "<<phi2[n][1]<<" "<<phi2[n][2]<<"\n";
    pfile.flush();
    pfile.close();
  }


  // Plots centerlines and nodal connections as a tecplot file
  void BiRod::PlotTec(const std::string filename) const
  {
    CoordConn MD;
    MD.nodes_element = 3;
    MD.spatial_dimension = 3;
    MD.nodes = 0;
    MD.elements = 0;
    MD.connectivity.clear();
    MD.coordinates.clear();
    const int nNodes = GetNumNodes();
    const auto& phi1 = Rod1->GetCenterline();
    const auto& phi2 = Rod2->GetCenterline();

    // Centerlines of the two rods
    MD.coordinates.resize(2*nNodes*3);
    for(int n=0; n<nNodes; ++n)
      for(int k=0; k<3; ++k)
	{ MD.coordinates[3*n+k] = phi1[n][k];
	  MD.coordinates[3*nNodes+3*n+k] = phi2[n][k]; }
    MD.nodes = 2*nNodes;

    // Elements
    for(int e=0; e<nNodes-1; ++e)
      {
	// Centerline of rod 1
	MD.connectivity.push_back( e+1 );
	MD.connectivity.push_back( e+2 );
	MD.connectivity.push_back( e+2 );

	// Centerline of rod 2
	MD.connectivity.push_back( nNodes+e+1 );
	MD.connectivity.push_back( nNodes+e+2 );
	MD.connectivity.push_back( nNodes+e+2 );

	// Connection between rods
	MD.connectivity.push_back( e+1 );
	MD.connectivity.push_back( e+1 );
	MD.connectivity.push_back( nNodes+e+1 );
      }
    
    // Last connection
    MD.connectivity.push_back( nNodes );
    MD.connectivity.push_back( nNodes );
    MD.connectivity.push_back( 2*nNodes );
    
    MD.elements = static_cast<int>(MD.connectivity.size()/3);
    PlotTecCoordConn(filename.c_str(), MD);
  }

  
  // Plots a specified rod centerline
  void BiRod::Plot(const std::string filename, const int rodnum) const
  {
    assert(rodnum==1 || rodnum==2);
    const Configuration& Rod = (rodnum==1) ? *Rod1 : *Rod2;
    const int nNodes = GetNumNodes();
    const auto& phi = Rod.GetCenterline();

    // Plot the centerline
    std::fstream pfile;
    pfile.open(filename.c_str(), std::ios::out);
    for(int n=0; n<nNodes; ++n)
      pfile<<phi[n][0]<<" "<<phi[n][1]<<" "<<phi[n][2]<<"\n";
    pfile.flush();
    pfile.close();
  }
  

  // Helper function to initialize a standard railway-track configuration
  void BiRod::InitializeRailTrackConfiguration()
  {
    const int nNodes = GetNumNodes();
    const int nElements = GetNumElements();
    const int nQuad = static_cast<int>(ElmArray[0]->GetIntegrationWeights(0).size());

    // Centerlines
    Vec3 phi1({0.,0.,0.});
    Vec3 phi2({0.,yoffset,0.});
    for(int n=0; n<nNodes; ++n)
      { phi1[2] = coordinates[n];
	phi2[2] = coordinates[n];
	Rod1->SetCenterline(n, phi1);
	Rod2->SetCenterline(n, phi2); }

    // Set rotations and vorticities
    Mat3 Rot, Omega;
    for(int i=0; i<3; ++i)
      {
	for(int j=0; j<3; ++j)
	  { Rot[i][j] = 0.;
	    Omega[i][j] = 0.; }
	Rot[i][i] = 1.;
      }
    for(int e=0; e<nElements; ++e)
      for(int q=0; q<nQuad; ++q)
	{ Rod1->SetRotation(e, q, Rot);
	  Rod1->SetVorticity(e, q, Omega);
	  Rod2->SetRotation(e, q, Rot);
	  Rod2->SetVorticity(e, q, Omega); }

    Rod1->SetInitialized();
    Rod2->SetInitialized();
    // -- done --
  }
  
}


    
