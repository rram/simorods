#ifndef OPS3_WORKSPACE_H
#define OPS3_WORKSPACE_H

#include <vector>
#include <NewPointKinematics.h>

namespace simorods
{

  //! Workspace for intermediate calculations required in Ops
  struct Ops3Workspace
  {
  private:
    const int nMaxVars; // Max. number of first variations- can be an estimate
    
  public:
    NewPointKinematics PD; // Kinematic data at a quadrature point
    std::vector<std::vector<NewVarPointKinematics>> varPD; //!< First variations
    
  public:
    // Constructor
    // \param[in] nvars max. number of first variations- can be an estimate
    inline Ops3Workspace(const int nvars)
      :nMaxVars(nvars),
      varPD(nvars, std::vector<NewVarPointKinematics>(nvars)) {}
    
    //! Destructor, does nothing
    inline virtual ~Ops3Workspace() {}

    //! Copy constructor
    //! \param[in] Obj Object to be copied. Only used for sizing
    inline Ops3Workspace(const Ops3Workspace& Obj)
      :nMaxVars(Obj.nMaxVars),
      varPD(Obj.nMaxVars, std::vector<NewVarPointKinematics>(Obj.nMaxVars)) {}
    
    //! Disable assignment
    Ops3Workspace& operator=(const Ops3Workspace&) = delete;
  };

}

#endif
