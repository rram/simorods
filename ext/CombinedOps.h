// Sriramajayam

#ifndef COMBINEDOPS_H
#define COMBINEDOPS_H

#include<SimoRodsOps.h>
#include<Ops3.h>
#include <cassert>

namespace simorods
{
  class CombinedOps: public DResidue
  {
  public:
    // constructor
    CombinedOps(const Element* elm,
		const std::vector<int> fieldnums,
		ConfigAccessStruct str,
		const Material& mat,
		OpsWorkspace* wrkspc1,
		OpsWorkspace* wrkspc2,
		Ops3Workspace* wrkspc3,
		const double kVal);
      
    // destructor, does nothing
    inline ~CombinedOps() {}
    
    // copy constructor
    CombinedOps(const CombinedOps& Obj);
    
    // Return the fields used
    inline const std::vector<int>& GetField() const 
    { return fields; }

    //! Cloning
    inline virtual CombinedOps* Clone() const override
    { return new CombinedOps(*this); }

    //! Disable assignment
    Ops& operator=(const Ops&) = delete;

    // Returns the Op1
    inline Ops GetOp1()
    { return Op1; }

    // Returns the Op2
    inline Ops GetOp2()
    { return Op2; }

    // Returns the Op3
    inline Ops3 GetOp3()
    { return Op3; }

    //! Returns the number of dofs for a given field
    inline int GetFieldDof(int fieldnumber) const override
    { return Op1.GetFieldDof(fieldnumber); }// initially it was Elm

    // Computes the energy functional
    double GetEnergy(const void* Config) const;

    // Residual calculation
    void GetVal(const void* Config,
		std::vector<std::vector<double>>* resval) const;
    
    // Residual and stiffness calculation
    void GetDVal(const void* Config, 
		 std::vector<std::vector<double>>* resval,
		 std::vector<std::vector<std::vector<std::vector<double>>>>* dresval) const;

    bool ConsistencyTest(const void* Config,
			 const double pertEPS, const double tolEPS) const
    {return true;}
    
    
  private:
    std::vector<int> fields; // fields for combined operation
    Ops Op1; // Operations for rod1
    Ops Op2; // Operations for rod2
    Ops3 Op3; // Operation for constrained system

    // Intermdiate variables
    mutable std::vector<std::vector<double>> resval_1, resval_2;
    mutable std::vector<std::vector<std::vector<std::vector<double>>>> dresval_1, dresval_2;
  };
}

#endif
