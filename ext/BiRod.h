// Sriramajayam

#ifndef SIMO_BIROD_CONFIGURATION_H
#define SIMO_BIROD_CONFIGURATION_H

#include <SimoRodsConfiguration.h>
#include <SimoRodsMaterial.h>
#include <SimoRodsOps.h>
#include <SimoRodsP11DElement.h>
#include <SimoRodsUtils.h>
#include <SimoRodsPointKinematics.h>
#include <Assembler.h>
#include <BiRodConfiguration.h>
#include <CombinedOps.h>
#include <PetscData.h>

namespace simorods
{
  //! Service class to manage simulation of birods
  //! Assumes that the initial configuration is straight
  //! The first rod is along Ez, the second is parallel to Ez with a y-offset
  //! The material of the two rods are assumed to be identical
  class BiRod
  {
  public:
    //! Constructor
    //! \param[in] coord Coordinates vector, copied
    //! \param[in] conn Connectivity array, copied
    //! \param[in] yoff Offset in the y-direction for the second rod
    //! \param[in] MC material constants
    //! \param[in] penalty Penalty parameter
    BiRod(const std::vector<double>& coord,
	  const std::vector<int>& conn,
	  const double yoff,
	  const MatConstants MC,
	  const double penalty);
    
    //! Disable copy
    BiRod(const BiRod&) = delete;

    //! Destructor- clean up data structures
    virtual ~BiRod();

    //! Access to coordinates
    const std::vector<double>& GetCoordinates() const;

    //! Access to connectivity
    const std::vector<int>& GetConnectivity() const;

    //! Access the number of elements
    int GetNumElements() const;

    //! Access the number of nodes
    int GetNumNodes() const;

    //! Access the configuration of rod 1
    Configuration& GetRod1();

    //! Access the configuration of rod 2
    Configuration& GetRod2();

    //! Access the element array
    const std::vector<Element*>& GetElementArray() const;

    //! Access the local to global map
    const LocalToGlobalMap& GetLocalToGlobalMap() const;

    //! Access the assembler
    const StandardAssembler<CombinedOps>& GetAssembler() const;

    //! Access the material
    const Material& GetMaterial() const;

    //! Access the penalty parameter
    double GetPenaltyParameter() const;
    
    //! Main functionality
    //! Solve based on a given set of Dirichlet bcs
    //! \param[in] nMaxIters Max number of iterations
    void Solve(const std::vector<int>& boundary,
	       const std::vector<double>& bvalues,
	       const int nMaxIters);

    //! Plots the centerlines and nodal connections
    void Plot(const std::string filename) const;

    //! Plots centerlines and nodal connections as a tecplot file
    void PlotTec(const std::string filename) const;

    //! Plots a specified rod centerline
    void Plot(const std::string filename, const int rodnum) const;

    //! Helper function to initialize a standard railway-track configuration
    void InitializeRailTrackConfiguration();
    
  private:
    //! Helper function to update the configuration of a rod
    void UpdateRod(const int rodnum, const double* sol);

    //! Helper function to destroy data structures
    void Destroy();
    
    const std::vector<double> coordinates; //!< Coordinates in the parametric domain
    const std::vector<int> connectivity; //!< Element connectivities
    const double yoffset; //!< Offset of the second rod from the first along the y-direction
    const Material Mtl; //!< Material of the rods
    const double PenaltyParam; //!< Penalty parameter
    std::vector<Element*> ElmArray; //!< Element array
    LocalToGlobalMap* L2GMap; //!< Local to global map
    std::vector<CombinedOps*> OpArray; //!< Operations array
    Configuration* Rod1; //!< Configuration of rod 1
    Configuration* Rod2; //!< Configuration of rod 2
    BiRodConfiguration* birod; //!< Combined configuration of both rods
    StandardAssembler<CombinedOps>* Asm; //!< Assembler
    PetscData PD; //!< Petsc data structures
    OpsWorkspace* WrkSpc1; //!< ops workspace for rod 1
    OpsWorkspace* WrkSpc2; //!< ops workspace for rod 2
    Ops3Workspace* WrkSpc3; //!< ops workspace for Ops3
  };

}

  
#endif
