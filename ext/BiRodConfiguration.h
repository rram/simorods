// Sriramajayam

#ifndef BIROD_CONFIGURATION_H
#define BIROD_CONFIGURATION_H

#include<SimoRodsConfiguration.h>
#include <cassert>

namespace simorods
{
  class BiRodConfiguration
  {
  public:
    BiRodConfiguration(const Configuration& r1,
		       const Configuration& r2,
		       const double& dist)
      :Rod1(r1),
      Rod2(r2),
      ConstraintDist(dist)
      {
	assert(ConstraintDist>-1.e-8);
	assert(Rod1.GetNumNodes()==Rod2.GetNumNodes());
	assert(Rod1.GetNumElements()==Rod2.GetNumElements());
	assert(Rod1.GetNumQuadraturePointsPerElement()==
	       Rod2.GetNumQuadraturePointsPerElement());
      }
    
    inline ~BiRodConfiguration() {}

    inline BiRodConfiguration(const BiRodConfiguration& obj)
      :Rod1(obj.Rod1),
      Rod2(obj.Rod2),
      ConstraintDist(obj.ConstraintDist) {}

    inline const Configuration& GetRod1() const
    { return Rod1; }

    inline const Configuration& GetRod2() const
    { return Rod2; }

    inline double GetConstraintDistance2() const
    { return ConstraintDist; }

    inline bool IsInitialized() const
    { return Rod1.IsInitialized() && Rod2.IsInitialized(); }
    
  private:
    const Configuration& Rod1;
    const Configuration& Rod2;
    const double ConstraintDist;
  };
}

#endif
