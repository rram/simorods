// Sriramajayam

// Example showing a rod being bent into a circle

#include <SimoRods_Module>
#include <cassert>
#include <iostream>
#include <PetscData.h>

using namespace simorods;

// Plot a given configuration
void PlotConfiguration(const std::string filename,
		       const Configuration& Config);

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
  
  // Create a 1D mesh over [0,1]
  const int nNodes = 20;
  const int nElements = nNodes-1;
  std::vector<double> coordinates(nNodes);
  std::vector<int> connectivity(nElements*2);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = static_cast<double>(n)/static_cast<double>(nNodes-1);
  for(int e=0; e<nElements; ++e)
    { connectivity[2*e] = e+1;
      connectivity[2*e+1] = e+2; }

  // Set the global coordinates array
  Segment<1>::SetGlobalCoordinatesArray(coordinates);

  // Create elements over the reference configuration
  std::vector<Element*> ElmArray(nElements);
  const int nFields = 6; // 3 centerline config + 3 for director increments
  for(int e=0; e<nElements; ++e)
    ElmArray[e] = new P11DElement<nFields>(connectivity[2*e], connectivity[2*e+1]);

  // Local to global map
  StandardP11DMap L2GMap(ElmArray);

  // Create a configuration of the rod
  const int nQuadsPerElm = static_cast<int>(ElmArray[0]->GetIntegrationWeights(0).size());
  Configuration Config(nNodes, nElements, nQuadsPerElm);

  // Initial configuration
  {
    // Set coordinates of the centerline. Reference config. assumed to be along E3
    Vec3 phival({0.,0.,0.}); 
    for(int n=0; n<nNodes; ++n)
      { phival[2] = coordinates[n];
	Config.SetCenterline(n, phival); }
    
    // Set rotations & vorticity
    Mat3 Rot, Omega;
    for(int i=0; i<3; ++i)
      {
	for(int j=0; j<3; ++j)
	  { Rot[i][j] = 0.;
	    Omega[i][j] = 0.; }
	Rot[i][i] = 1.;
      }
    for(int e=0; e<nElements; ++e)
      for(int q=0; q<nQuadsPerElm; ++q)
	{ Config.SetRotation(e, q, Rot);
	  Config.SetVorticity(e, q, Omega); }
  }
  Config.SetInitialized();
  PlotConfiguration("ref.dat", Config);
    
  // Create material
  MatConstants MC(1., 1., 0.1, 0.1); // GA, EA, EI, GJ
  Material SMat(MC);
  
  // Create operations
  std::vector<Ops*> OpArray(nElements);
  for(int e=0; e<nElements; ++e)
    {
      // How to access the configuration 
      ConfigAccessStruct str(std::vector<int>({connectivity[2*e]-1, connectivity[2*e+1]-1}),
			     e, nQuadsPerElm);
      // Create operation
      OpArray[e] = new Ops(ElmArray[e],str, SMat);
    }
  
  // Create assembler
  StandardAssembler<Ops> Asm(OpArray, L2GMap);

  // Create PETSc data structures
  PetscData PD;
  // Estimate number of nonzeros in each row
  std::vector<int> nnz(L2GMap.GetTotalNumDof());
  Asm.CountNonzeros(nnz); 
  PD.Initialize(nnz);

  // Set a direct solver
  PC pc;
  KSPGetPC(PD.kspSOLVER, &pc);
  PCSetType(pc, PCLU);

  // Dirichlet boundary conditions
  std::vector<int> boundary;
  // Fix the left end
  for(int i=0; i<6; ++i)
    { boundary.push_back(0*nFields+i); }
  // Displace the right end
  boundary.push_back((nNodes-1)*nFields+0);
  const Vec3 dy({0.05,0.,0.});
  std::vector<double> bvalues(boundary.size());
  std::fill(bvalues.begin(), bvalues.end(), 0.);

  // Load stepping
  for(int step=0; step<1; ++step)
    {
      // Displace the right end
      Config.UpdateCenterline(nNodes-1, dy);
      Config.SetInitialized();
      
      // Newton-Raphson
      int iter = 0;
      while(true)
	{
	  std::cout<<"\nIteration "<<++iter<<": "<<std::flush;

	  // Assembe the matrix-vector system at the current configuration
	  Asm.Assemble(&Config, PD.resVEC, PD.stiffnessMAT);

	  // Set Dirichlet BCs
	  PD.SetDirichletBCs(boundary, bvalues);
 
	  // Solve
	  PD.Solve();

	  // Check convergence
	  if(PD.HasConverged(1.e-10, 1., 1.))
	    { std::cout<<"\nConverged!\n"<<std::flush; break; }

	  // Correct the sign of increments
	  VecScale(PD.solutionVEC, -1.);

	  // Update centerline
	  for(int n=0; n<nNodes; ++n)
	    { int indx[] = {nFields*n, nFields*n+1, nFields*n+2};
	      Vec3 dphival;
	      VecGetValues(PD.solutionVEC, 3, indx, &dphival[0]);
	      Config.UpdateCenterline(n, dphival); }

	  // Update rotations and vorticity
	  for(int e=0; e<nElements; ++e)
	    { const int i1 = connectivity[2*e]-1;
	      const int i2 = connectivity[2*e+1]-1;
	      int indx[] = {nFields*i1+3, nFields*i1+4, nFields*i1+5,
			    nFields*i2+3, nFields*i2+4, nFields*i2+5};
	      double thetadofs[6];
	      VecGetValues(PD.solutionVEC, 6, indx, thetadofs);

	      // Quadrature-point-wise-update
	      Vec3 theta, dtheta;
	      for(int q=0; q<nQuadsPerElm; ++q)
		{ theta[0] = theta[1] = theta[2] = 0.;
		  dtheta[0] = dtheta[1] = dtheta[2] = 0.;
		  for(int a=0; a<2; ++a)
		    for(int k=0; k<3; ++k)
		      { theta[k] += thetadofs[3*a+k]*ElmArray[e]->GetShape(0,q,a);
			dtheta[k] += thetadofs[3*a+k]*ElmArray[e]->GetDShape(0,q,a,0); }
		  Config.UpdateRotations(e, q, theta, dtheta); }
	    }
	  Config.SetInitialized();
	} // End Newton-Raphson

      // Plot the deformed configuration
      PlotConfiguration("def.dat", Config);
      
    } // End load stepping
  
  // Clean up
  for(auto& x:ElmArray) delete x;
  for(auto& x:OpArray) delete x;
  PD.Destroy();
  
  // Finalize PETSc
  PetscFinalize();
}


// Plot a given configuration
void PlotConfiguration(const std::string filename,
		       const Configuration& Config)
{
  std::fstream pfile;
  pfile.open(filename.c_str(), std::ios::out);
  assert(pfile.good());
  const int nNodes = Config.GetNumNodes();
  const auto& phi = Config.GetCenterline();
  for(int n=0; n<nNodes; ++n)
    pfile<<phi[n][0]<<"\t"<<phi[n][1]<<"\t"<<phi[n][2]<<"\n";
  pfile.flush();
  pfile.close();
}
