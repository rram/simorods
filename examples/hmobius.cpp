// Sriramajayam

#include <SimoRods_Module>
#include <cassert>
#include <iostream>
#include <PetscData.h>
#include <HermiteElement.h>
#include <L2GMapForHermiteElement.h>
#include <PlottingUtils.h>

using namespace simorods;

// Plot quadrature point coordinates and the Lambda values
// Plot quadrature point coordinates and the Lambda values
void PlotCenterline(const std::string filename,
		    const std::vector<double>& cooridnates, 
		    const Configuration& Config);

// Plot a given configuration
void PlotTecConfiguration(const std::string filename,
			  const double width,
			  const Configuration& Config);

// Plot components of the vorticity
void PlotVorticity(const std::string filename,
		   const Configuration& Config,
		   const std::vector<Element*>& ElmArray);

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
  
  // Create a 1D mesh over [0,30]
  const int nNodes = 400;
  const int nElements = nNodes-1;
  std::vector<double> coordinates(nNodes);
  std::vector<int> connectivity(nElements*2);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 30.*static_cast<double>(n)/static_cast<double>(nNodes-1);
  for(int e=0; e<nElements; ++e)
    { connectivity[2*e] = e+1;
      connectivity[2*e+1] = e+2; }

  // Set the global coordinates array
  Segment<1>::SetGlobalCoordinatesArray(coordinates);

  // Create elements over the reference configuration
  std::vector<Element*> ElmArray(nElements);
  const int nFields = 6; // 3 centerline config + 3 for director increments
  for(int e=0; e<nElements; ++e)
    ElmArray[e] = new HermiteElement<nFields>(connectivity[2*e], connectivity[2*e+1]);

  // Local to global map
  L2GMapForHermiteElement L2GMap(ElmArray);
  const int nDofNodes = L2GMap.GetTotalNumDof()/nFields;
  assert(nDofNodes==2*nNodes);
  
  // Create a configuration of the rod
  const int nQuadsPerElm = static_cast<int>(ElmArray[0]->GetIntegrationWeights(0).size());
  Configuration Config(nDofNodes, nElements, nQuadsPerElm);

  // Initial configuration
  {
    // Set coordinates of the centerline. Reference config. assumed to be along E3
    Vec3 phival({0.,0.,0.}); 
    for(int n=0; n<nNodes; ++n)
      { phival[2] = coordinates[n];
	Config.SetCenterline(n, phival); }

    // Dofs corresponding to derivatives
    for(int n=0; n<nNodes; ++n)
      { phival[2] = 1.; 
	Config.SetCenterline(nNodes+n, phival); }
    
    // Set rotations & vorticity
    Mat3 Rot, Omega;
    for(int i=0; i<3; ++i)
      {
	for(int j=0; j<3; ++j)
	  { Rot[i][j] = 0.;
	    Omega[i][j] = 0.; }
	Rot[i][i] = 1.;
      }
    for(int e=0; e<nElements; ++e)
      for(int q=0; q<nQuadsPerElm; ++q)
	{ Config.SetRotation(e, q, Rot);
	  Config.SetVorticity(e, q, Omega); }
  }
  Config.SetInitialized();
 
  // Create material
  const double E = 1.;
  const double G = 0.5*E/(1.4);
  const double w = 2.0*30./(2.*M_PI); // 0.4, 1.0, 1.6, 2.0, 3.0
  const double h = 0.018;
  const double EA = E*w*h;
  const double GA1 = G*w*h;
  const double GA2 = G*w*h;
  const double EI1 = E*w*h*h*h/12.;
  const double EI2 = E*h*w*w*w/12.;
  const double aval = w/2.;
  const double bval = h/2.;
  const double GJ = G*aval*std::pow(bval,3.)*(16./3.-3.361*bval/aval);
  MatConstants MC(GA1, GA2, EA, EI1, EI2, GJ);
  Material SMat(MC);
  PlotTecConfiguration("ref.tec", w, Config);
  
  // Create operations
  std::vector<Ops*> OpArray(nElements);
  for(int e=0; e<nElements; ++e)
    {
      // How to access the configuration
      std::vector<int> dofnums(4);
      for(int a=0; a<4; ++a)
	dofnums[a] = L2GMap.Map(0,a,e)/nFields;
      ConfigAccessStruct str(dofnums, e, nQuadsPerElm);
      // Create operation
      OpArray[e] = new Ops(ElmArray[e], str, SMat);
    }
  
  // Create assembler
  StandardAssembler<Ops> Asm(OpArray, L2GMap);

  // Create PETSc data structures
  PetscData PD;
  // Estimate number of nonzeros in each row
  std::vector<int> nnz(L2GMap.GetTotalNumDof());
  Asm.CountNonzeros(nnz); 
  PD.Initialize(nnz);

  // Set a direct solver
  PC pc;
  KSPGetPC(PD.kspSOLVER, &pc);
  PCSetType(pc, PCLU);

  // Dirichlet boundary conditions
  std::vector<int> boundary;
  // Fix the left end
  for(int i=0; i<6; ++i)
    { boundary.push_back(0*nFields+i); }
  // Rotate the right end about the X-axis
  boundary.push_back((nNodes-1)*nFields+3);
  assert(L2GMap.Map(0,1,nElements-1)/nFields==nNodes-1);
  const double dThetaX = 10.*M_PI/180.;
  std::vector<double> bvalues(boundary.size());
  std::fill(bvalues.begin(), bvalues.end(), 0.);

  // Load stepping
  std::string filename;
  for(int step=0; step<36; ++step)
    {
      // Newton-Raphson
      int iter = 0;
      while(true)
	{
	  std::cout<<"\nIteration "<<++iter<<": "<<std::flush;

	  // Assembe the matrix-vector system at the current configuration
	  Asm.Assemble(&Config, PD.resVEC, PD.stiffnessMAT);

	  // Impose the boundary condition at the first step alone
	  std::fill(bvalues.begin(), bvalues.end(), 0.);
	  if(iter==1)
	    { bvalues[6] =  dThetaX; }
	  
	  // Set Dirichlet BCs
	  PD.SetDirichletBCs(boundary, bvalues);
 
	  // Solve
	  PD.Solve();

	  // Check convergence
	  if(PD.HasConverged(1.e-10, 1., 1.))
	    { std::cout<<"\nConverged!\n"<<std::flush; break; }

	  // Correct the sign of increments
	  VecScale(PD.solutionVEC, -1.);

	  // Update centerline dofs
	  for(int n=0; n<nDofNodes; ++n)
	    { int indx[] = {nFields*n, nFields*n+1, nFields*n+2};
	      Vec3 dphival;
	      VecGetValues(PD.solutionVEC, 3, indx, &dphival[0]);
	      Config.UpdateCenterline(n, dphival); }

	  // Update rotations and vorticity
	  double* solArray;
	  VecGetArray(PD.solutionVEC, &solArray);
	  for(int e=0; e<nElements; ++e)
	    {
	      // Quadrature-point-wise-update
	      Vec3 theta, dtheta;
	      for(int q=0; q<nQuadsPerElm; ++q)
		{ theta[0] = theta[1] = theta[2] = 0.;
		  dtheta[0] = dtheta[1] = dtheta[2] = 0.;
		  for(int a=0; a<4; ++a)
		    for(int k=0; k<3; ++k)
		      { theta[k] += solArray[L2GMap.Map(k+3,a,e)]*ElmArray[e]->GetShape(0,q,a);
			dtheta[k] += solArray[L2GMap.Map(k+3,a,e)]*ElmArray[e]->GetDShape(0,q,a,0); }
		  Config.UpdateRotations(e, q, theta, dtheta); }
	    }
	  Config.SetInitialized();
	  VecRestoreArray(PD.solutionVEC, &solArray);
	} // End Newton-Raphson

      // Plot the deformed configuration
      filename = "def-" + std::to_string(step) + ".tec";
      PlotTecConfiguration(filename, w, Config);
      
    } // End load stepping

  // Plot quadrature point coordinates and the Lambda values
  PlotCenterline("bent.dat", coordinates, Config);
  
  // Plot components of the vorticity
  PlotVorticity("omega-bend.dat", Config, ElmArray);
  
  // Twisting
  boundary.clear();
  for(int i=0; i<6; ++i)
    { boundary.push_back(0*nFields+i); }
  for(int i=0; i<6; ++i)
    { boundary.push_back( nFields*(nNodes-1)+i); }
  bvalues.clear();
  bvalues.resize(boundary.size());
  const double dThetaZ = 0.5*M_PI/180.;
  for(int step=0; step<360; ++step)
    {
      // Newton-Raphson
      int iter = 0;
      while(true)
	{
	  std::cout<<"\nIteration "<<++iter<<": "<<std::flush;

	  // Assembe the matrix-vector system at the current configuration
	  Asm.Assemble(&Config, PD.resVEC, PD.stiffnessMAT);

	  // Impose the boundary condition at the first step alone
	  std::fill(bvalues.begin(), bvalues.end(), 0.);
	  if(iter==1)
	    { bvalues[bvalues.size()-1] =  dThetaZ; }
	  
	  // Set Dirichlet BCs
	  PD.SetDirichletBCs(boundary, bvalues);
 
	  // Solve
	  PD.Solve();

	  // Check convergence
	  if(PD.HasConverged(1.e-10, 1., 1.))
	    { std::cout<<"\nConverged!\n"<<std::flush; break; }

	  // Correct the sign of increments
	  VecScale(PD.solutionVEC, -1.);
	  
	  // Update centerline dofs
	  for(int n=0; n<nDofNodes; ++n)
	    { int indx[] = {nFields*n, nFields*n+1, nFields*n+2};
	      Vec3 incphival;
	      VecGetValues(PD.solutionVEC, 3, indx, &incphival[0]);
	      Config.UpdateCenterline(n, incphival); }

	  // Update rotations and vorticity
	  double* solArray;
	  VecGetArray(PD.solutionVEC, &solArray);
	  for(int e=0; e<nElements; ++e)
	    {
	      // Quadrature-point-wise-update
	      Vec3 theta, dtheta;
	      for(int q=0; q<nQuadsPerElm; ++q)
		{ theta[0] = theta[1] = theta[2] = 0.;
		  dtheta[0] = dtheta[1] = dtheta[2] = 0.;
		  for(int a=0; a<4; ++a)
		    for(int k=0; k<3; ++k)
		      { theta[k] += solArray[L2GMap.Map(k+3,a,e)]*ElmArray[e]->GetShape(0,q,a);
			dtheta[k] += solArray[L2GMap.Map(k+3,a,e)]*ElmArray[e]->GetDShape(0,q,a,0); }
		  Config.UpdateRotations(e, q, theta, dtheta); }
	    }
	  Config.SetInitialized();
	  VecRestoreArray(PD.solutionVEC, &solArray);
	} // End Newton-Raphson

      // Plot the deformed configuration
      filename = "def-t-" + std::to_string(step) + ".tec";
      PlotTecConfiguration(filename, w, Config);
      
    } // End load stepping

  // Plot components of the vorticity
  PlotVorticity("omega-twist.dat", Config, ElmArray);
  

  // Plot quadrature point coordinates and the Lambda values
  PlotCenterline("twisted.dat", coordinates, Config);

  // Decrease thickness
  std::fill(bvalues.begin(), bvalues.end(), 0.);
  for(int step=0; step<=50; ++step)
    {
      std::cout<<"\n\nMaterial step "<<step<<std::flush;
      const double factor = 1./std::pow(1.2, static_cast<double>(step)); // Scaling for h. Apply to h^2 terms
      const double step_EI1 = MC.EI1*factor*factor;
      const double step_GJ = G*aval*factor*factor*std::pow(bval,3.)*(16./3.-3.361*factor*bval/aval);
      MatConstants step_MC(GA1, GA2, EA, step_EI1, EI2, step_GJ);
      SMat.SetMaterialConstants(step_MC);

      // Newton-Raphson
      int iter = 0;
      while(true)
	{
	  std::cout<<"\nIteration "<<++iter<<": "<<std::flush;

	  // Assembe the matrix-vector system at the current configuration
	  Asm.Assemble(&Config, PD.resVEC, PD.stiffnessMAT);

	  // Set Dirichlet BCs
	  PD.SetDirichletBCs(boundary, bvalues);
 
	  // Solve
	  PD.Solve();

	  // Check convergence
	  if(PD.HasConverged(1.e-10, 1., 1.))
	    { std::cout<<"\nConverged!\n"<<std::flush; break; }

	  // Correct the sign of increments
	  VecScale(PD.solutionVEC, -1.);

	  // Update centerline dofs
	  for(int n=0; n<nDofNodes; ++n)
	    { int indx[] = {nFields*n, nFields*n+1, nFields*n+2};
	      Vec3 incphival;
	      VecGetValues(PD.solutionVEC, 3, indx, &incphival[0]);
	      Config.UpdateCenterline(n, incphival); }
	  
	  // Update rotations and vorticity
	  double* solArray;
	  VecGetArray(PD.solutionVEC, &solArray);
	  for(int e=0; e<nElements; ++e)
	    {
	      // Quadrature-point-wise-update
	      Vec3 theta, dtheta;
	      for(int q=0; q<nQuadsPerElm; ++q)
		{ theta[0] = theta[1] = theta[2] = 0.;
		  dtheta[0] = dtheta[1] = dtheta[2] = 0.;
		  for(int a=0; a<4; ++a)
		    for(int k=0; k<3; ++k)
		      { theta[k] += solArray[L2GMap.Map(k+3,a,e)]*ElmArray[e]->GetShape(0,q,a);
			dtheta[k] += solArray[L2GMap.Map(k+3,a,e)]*ElmArray[e]->GetDShape(0,q,a,0); }
		  Config.UpdateRotations(e, q, theta, dtheta); }
	    }
	  Config.SetInitialized();
	  VecRestoreArray(PD.solutionVEC, &solArray);
	} // End Newton-Raphson

      // Plot the deformed configuration
      filename = "def-h-" + std::to_string(step) + ".tec";
      PlotTecConfiguration(filename, w, Config);

      filename = "centerline-" + std::to_string(step) + ".dat";
      PlotCenterline(filename, coordinates, Config);

      // Plot components of the vorticity
      filename = "omega-" + std::to_string(step) + ".dat";
      PlotVorticity(filename, Config, ElmArray);

      
    } // End material stepping

  // Clean up
  for(auto& x:ElmArray) delete x;
  for(auto& x:OpArray) delete x;
  PD.Destroy();
  
  // Finalize PETSc
  PetscFinalize();
}

// Plot quadrature point coordinates and the Lambda values
void PlotCenterline(const std::string filename,
		    const std::vector<double>& coordinates, 
		    const Configuration& Config)
{
  const auto& phi = Config.GetCenterline();
  const int nNodes = static_cast<int>(coordinates.size());
  std::fstream pfile;
  pfile.open(filename.c_str(), std::ios::out);
  for(int n=0; n<nNodes; ++n)
    pfile<<phi[n][0]<<" "<<phi[n][1]<<" "<<phi[n][2]<<" "<<coordinates[n]<<"\n";
  pfile.close();
  return;
}



// Plot a given configuration
void PlotTecConfiguration(const std::string filename,
			  const double width, 
			  const Configuration& Config)
{
  const int nNodes = Config.GetNumElements()+1;
  const int nQuad = Config.GetNumQuadraturePointsPerElement();
  
  // Compute nodal rotations
  std::vector<double> tvec(3*nNodes);
  const double Ex[] = {1.,0.,0.};
  std::fill(tvec.begin(), tvec.end(), 0.);
  for(int n=0; n<nNodes; ++n)
    {
      Mat3 Lambda = (n==nNodes-1) ? Config.GetRotations(n-1,nQuad-1) : Config.GetRotations(n,0);
      for(int i=0; i<3; ++i)
	for(int j=0; j<3; ++j)
	  tvec[3*n+i] += Lambda[i][j]*Ex[j];
    }

  // Create a quad mesh
  CoordConn MD;
  MD.nodes_element = 4;
  MD.spatial_dimension = 3;
  MD.nodes = 3*nNodes;
  MD.coordinates.resize(3*MD.nodes);
  MD.elements = 2*(nNodes-1);
  MD.connectivity.clear();

  // Coordinates
  const auto& phi = Config.GetCenterline();
  for(int n=0; n<nNodes; ++n)
    {
      const auto X = phi[n];
      const double* dir = &tvec[3*n];
      for(int k=0; k<3; ++k)
	{
	  MD.coordinates[3*n+k] = X[k];
	  MD.coordinates[3*(nNodes)+3*n+k] = X[k]+0.5*width*dir[k];
	  MD.coordinates[3*2*(nNodes)+3*n+k] = X[k]-0.5*width*dir[k];
	}
    }

  // Connectivity
  for(int e=0; e<nNodes-1; ++e)
    {
      int pconn[] = {e+1, e+2, e+(nNodes)+2, e+(nNodes)+1};
      int mconn[] = {e+2, e+1, e+2*(nNodes)+1, e+2*(nNodes)+2};
      for(int a=0; a<4; ++a)
	MD.connectivity.push_back(pconn[a]);
      for(int a=0; a<4; ++a)
	MD.connectivity.push_back(mconn[a]);
    }
  PlotTecCoordConn(filename.c_str(), MD, (char*)"QUADRILATERAL");
}


// Plot components of the vorticity
void PlotVorticity(const std::string filename,
		   const Configuration& Config,
		   const std::vector<Element*>& ElmArray)
{
  std::fstream pfile;
  pfile.open(filename.c_str(), std::ios::out);
  const int nElements = Config.GetNumElements();
  const int nQuad = Config.GetNumQuadraturePointsPerElement();
  for(int e=0; e<nElements; ++e)
    {
      const auto& Qpts = ElmArray[e]->GetIntegrationPointCoordinates(0);
      for(int q=0; q<nQuad; ++q)
	{
	  const auto& Omega = Config.GetVorticity(e, q);
	  double omega[] = {Omega[2][1], Omega[0][2], Omega[1][0]};
	  pfile<<Qpts[q]<<" "<<omega[0]<<" "<<omega[1]<<" "<<omega[2]<<"\n";
	}
    }
  pfile.flush();
  pfile.close();
}
